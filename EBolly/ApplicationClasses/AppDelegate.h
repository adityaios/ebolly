//
//  AppDelegate.h
//  EBolly
//
//  Created by Yashvir on 31/07/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <GooglePlus/GooglePlus.h>
#import <FacebookSDK/FacebookSDK.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import "MMDrawerController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic,strong) MMDrawerController * drawerController; // mmDrawer
@property (strong, nonatomic) UINavigationController * homeNavController;
@property (strong, nonatomic)     id parentNavController;
@property () BOOL restrictRotation; // orientation layout

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

+ (AppDelegate *)appDelegate;
+(UIWindow *) getWindowInstance;

-(void) getRootNavController;
-(void) getRootNavigationController;


@end

