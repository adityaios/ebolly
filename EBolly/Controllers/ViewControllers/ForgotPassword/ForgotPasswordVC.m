//
//  ForgotPasswordVC.m
//  EBolly
//
//  Created by Yashvir on 11/08/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "ForgotPasswordCell.h"
#import "TSMessage.h"
#import "ForgotPasswordReq.h"
#import "Network.h"
#import "Loader.h"
#import "AppDelegate.h"

static const int KTextFieldUsernameEmail = 1;
static const int KBtnSend = 2;

@interface ForgotPasswordVC ()<NetworkDelegate>
{
    Network * network ;
}
@property (weak, nonatomic) IBOutlet UITableView *tableVIew;

@end

@implementation ForgotPasswordVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self initializeVariables];
//    [self viewSetUp];
    [self restrictRotation:YES];
}

#pragma mark - Layout Orientation
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void) restrictRotation:(BOOL) restriction
{
    AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.restrictRotation = restriction;
}


#pragma mark - SetUpView

- (void) initializeVariables
{
    network = [Network new];
    network.delegate = self;
    
    [self addBackBarBtn];
}

-(void) addBackBarBtn
{
    UIButton * backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 50, 20)];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundColor:[UIColor clearColor]];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"OpenSans-CondensedBold" size:15.0]];
    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
}

-(void)back
{
    [self restrictRotation:YES];// set orientation
    [self.navigationController popViewControllerAnimated:YES];
}


-(void) setupView
{

}


#pragma mark - TableView Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ForgotPasswordCell * cell = [_tableVIew dequeueReusableCellWithIdentifier:@"Cell"];
    return cell;
}

-(ForgotPasswordCell *) getForgotPasswordCellInstance
{
    return  (ForgotPasswordCell *)[_tableVIew cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

#pragma mark - Send Button Clicked
- (IBAction)btnClickedSend:(id)sender
{
    UITextField * emailTextField = (UITextField *)[[self getForgotPasswordCellInstance] viewWithTag:KTextFieldUsernameEmail];
    
    if (emailTextField.text.length > 0 && ![emailTextField.text isEqualToString:@" "])
    {
        // call Forgot Password request api.
        ForgotPasswordReq * req = [[ForgotPasswordReq alloc] init];
        req.emailIDOrUsername = emailTextField.text;

        // make API hit.
        [network forgotPasswordWS:req  UserInfo:[[NSDictionary alloc] initWithObjectsAndKeys:KForgotPasswordAction,KAction, nil]];
    }
    else
    {
        [TSMessage showNotificationInViewController:self title:@"EmailID/Username not entered!" subtitle:@"" type:TSMessageNotificationTypeError];
    }
}

#pragma mark - Network Delegate - Success
-(void) didReceiveResponse:(NSDictionary *)response withUserInfo:(NSDictionary *)userInfo
{
    if ([[response objectForKey:KStatus] integerValue] == 1)
    {
        if ([[response objectForKey:KStatusCode] integerValue] == 1)
        {
            // successful
            if([[userInfo objectForKey:KAction] isEqualToString:KForgotPasswordAction])
            {
                [TSMessage showNotificationInViewController:self title:@"An Email has been sent on your registered emailID through which you can reset your password." subtitle:@"" type:TSMessageNotificationTypeSuccess];
            }
        }
        else
        {
        // failure
            if([[userInfo objectForKey:KAction] isEqualToString:KForgotPasswordAction])
            {
                [TSMessage showNotificationInViewController:self title:[response objectForKey:KMsg] subtitle:@"" type:TSMessageNotificationTypeWarning];
            }
   
        }
    }
    else
    {
        [TSMessage showNotificationInViewController:self title:@"Sorry! Something went wrong." subtitle:@"" type:TSMessageNotificationTypeError];
    }
}

#pragma mark - Network Delegate - failkure
- (void) didFailWithError:(NSError *)error withUserInfo:(NSDictionary *)userInfo
{
    if (error.code == 3840)
    {
        [TSMessage showNotificationInViewController:self title:@"Error" subtitle:@"Sorry! Something went wrong!" type:TSMessageNotificationTypeError duration:2.0];
        
    }
    else if (error.code == -1004)
    {
        UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"No Internet Connection" message:@"Please check your internet connection." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Settings", nil];
        alertView.tag = 1002;
        [alertView show];
    }
    else
    {
        [TSMessage showNotificationInViewController:self title:@"Sorry Something went wrong!" subtitle:@"Please try again." type:TSMessageNotificationTypeError duration:2.0];
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
