//
//  HomeCell.m
//  EBolly
//
//  Created by Yashvir on 08/08/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import "HomeCell.h"

@implementation HomeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
