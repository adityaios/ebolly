//
//  HomeVC.m
//  EBolly
//
//  Created by Yashvir on 08/08/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import "HomeVC.h"
#import "UserDefault.h"
#import "AppDelegate.h"
#import "MMDrawerBarButtonItem.h"
#import "UIViewController+MMDrawerController.h"

@interface HomeVC ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation HomeVC

- (void)viewDidLoad
{
    [super viewDidLoad];

  
    [self initializeVariables];
    [self viewSetUp];
    [self restrictRotation:YES];
    
    [self embedWebsite];
}

- (IBAction)btnClickedBack:(id)sender
{
    [_webView goBack];
}

- (IBAction)btnClickedForward:(id)sender
{
    [_webView goForward];
}

#pragma mark - Layout Orientation
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void) restrictRotation:(BOOL) restriction
{
    AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.restrictRotation = restriction;
}


#pragma mark - SetUpView

- (void) initializeVariables
{
    AppDelegate* sharedDelegate = [AppDelegate appDelegate];
    sharedDelegate.homeNavController = self.navigationController;
}

#pragma mark - Reload webpage Button Clicked
- (IBAction)btnClickedReloadPage:(id)sender
{
    [_activityIndicator setHidden:NO];
    [_activityIndicator startAnimating];
     [_webView reload];
}

- (void) viewSetUp
{
    UITapGestureRecognizer * twoFingerDoubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(twoFingerDoubleTap:)];
    [twoFingerDoubleTap setNumberOfTapsRequired:2];
    [twoFingerDoubleTap setNumberOfTouchesRequired:2];
    [self.view addGestureRecognizer:twoFingerDoubleTap];
    
    [self setupRightMenuButton];
    
    self.title = NSLocalizedString(@"Home", @"Home");
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]}; //set color of navigation bar title color.
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageNamed:@"navBarStrip.jpg"]
                                                                 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch] forBarMetrics:UIBarMetricsDefault];
}


- (void)setupLeftMenuButton
{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}


- (void)setupRightMenuButton
{
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    [rightDrawerButton setTintColor:[UIColor whiteColor]];
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}


#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

-(void)doubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideLeft completion:nil];
}

-(void)twoFingerDoubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideRight completion:nil];
}




- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)embedWebsite
{
    self.webView.backgroundColor = [UIColor grayColor];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.empowerbollywood.com/beta/"]]];
}

#pragma WebView Delegate Methods
// This function is called on all location change :
- (BOOL)webView:(UIWebView *)webView2 shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_activityIndicator startAnimating];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_activityIndicator stopAnimating];
    [_activityIndicator setHidden:YES];
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [_activityIndicator stopAnimating];
    [_activityIndicator setHidden:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
