//
//  LoginCell.h
//  EBolly
//
//  Created by Yashvir on 31/07/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>

@interface LoginCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *textFieldUsername;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnOutletSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnOutletForgotPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnOutletForgotUsername;
@property (weak, nonatomic) IBOutlet GPPSignInButton *btnOutletGooglePlus;

@end
