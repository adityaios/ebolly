//
//  ViewController.m
//  EBolly
//
//  Created by Yashvir on 31/07/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import "ViewController.h"
#import "LoginCell.h"
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <FacebookSDK/FacebookSDK.h>
#import "Loader.h"
#import "Network.h"
#import "AppDelegate.h"
#import "LoginReq.h"
#import "HomeVC.h"
#import "RegistrationVC.h"
#import "UserDefault.h"
#import "MenuVC.h"
#import "TSMessage.h"
#import "ForgotPasswordVC.h"

#import "MMDrawerController.h" // drawer
#import "MMDrawerVisualState.h" // drawer
#import "MMExampleDrawerVisualStateManager.h" // drawer
#import "MMNavigationController.h" // drawer

@interface ViewController ()<NetworkDelegate, GPPSignInDelegate>
{
    NSMutableDictionary * socialUserDict; // Facebook user Details Dict
    Network * network;
    float keyboardFrameHeight;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initializeVariables];
    [self viewSetUp];
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    [[NSUserDefaults standardUserDefaults] setObject:KStandardRegister forKey:KLoginType];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:KSocialType];
//    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeKeyboardNotifications];
}

#pragma mark - Tableview Datasource Methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.view.frame.size.height;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LoginCell * cell = (LoginCell *)[_tableView dequeueReusableCellWithIdentifier:@"Cell"];
    return cell;
}



#pragma mark - Navigate to registration Screen
-(void) takeUserToRegistrationScreen:(NSDictionary *) userDetailDict registrationType:(NSString *)registrationType
{
    
     RegistrationVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"RegistrationVCID"];
        vc.userDetailDict = userDetailDict;
    //    vc.registrationType = KSocialRegister;
    [self.navigationController pushViewController:vc animated:YES];
    
}


-(void) checkForSocialLogin:(NSString *) userId Provider: (NSString *)provider
{
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:KLoginType] isEqualToString:KSocialRegister])
    {
        [Loader showLoader];
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:KSocialType] isEqualToString:KFacebook])
        {
            [network checkSocailRegistration:[[NSDictionary alloc] initWithObjectsAndKeys:KSocialLoginCheckAction,KAction,userId,KFBId, nil] UserInfo:[[NSDictionary alloc] initWithObjectsAndKeys:KSocialLoginCheckAction,KAction,userId,KFBId, nil]];
        }
        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:KSocialType] isEqualToString:KGooglePlus])
        {
            [network checkSocailRegistration:[[NSDictionary alloc] initWithObjectsAndKeys:KSocialLoginCheckAction,KAction,userId,KGPlusId, nil] UserInfo:[[NSDictionary alloc] initWithObjectsAndKeys:KSocialLoginCheckAction,KAction,userId,KGPlusId, nil]];
        }
        else
        {
            [Loader hideLoader];
        }
    }
    else
    {
        [Loader hideLoader];
    }
}




#pragma mark - SetUpView
- (void) initializeVariables
{
    network = [[Network alloc] init];
    network.delegate = self;
 
//    @try
//    {
//        if ([[NSUserDefaults standardUserDefaults] objectForKey:KBucketName] == nil)
//        {
//            if ([network getBucketURL] == nil || [network getBucketURL].length < 1 )
//            {
//                [network getBucketNameFromServer:[[NSDictionary alloc] initWithObjectsAndKeys:KGetBucketUrlAction,kAction, nil] UserInfo:[[NSDictionary alloc] initWithObjectsAndKeys:KGetBucketUrlAction,KActionkey,nil]];
//            }
//        }
//    }
//    @catch (NSException *exception)
//    {
//        NSLog(@"%@", exception.debugDescription);
//    }
    
}


- (void) viewSetUp
{
    self.title = NSLocalizedString(@"Login", @"Login");
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]}; //set color of navigation bar title color.
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageNamed:@"navBarStrip.jpg"]
                                                                 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch] forBarMetrics:UIBarMetricsDefault];
    
    [[self getLoginCellInstance].btnOutletForgotPassword.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [[self getLoginCellInstance].btnOutletForgotUsername.titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    [self setUpGooglePlus];
}


#pragma mark - Keyboard Notifications
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}


- (void)removeKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)keyboardWasShown:(NSNotification*)aNotification
{
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonClicked)];
    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
    
    NSDictionary *userInfo = [aNotification userInfo];
    CGRect keyboardEndFrame;
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    keyboardFrameHeight = [self.view convertRect:keyboardEndFrame toView:nil].size.height;
    
    self.tableView.contentSize = CGSizeMake(self.tableView.frame.size.width, (self.tableView.contentSize.height + keyboardFrameHeight));
    
}


- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:nil];
    
    UIEdgeInsets insets = self.tableView.contentInset;
    insets.bottom = 0;
    self.tableView.contentInset = insets;
    
    self.tableView.contentSize = CGSizeMake(self.tableView.frame.size.width, (self.tableView.contentSize.height - keyboardFrameHeight));
    
}

#pragma mark - Done Btn Clicked
-(void)doneButtonClicked
{
    [self.view endEditing:YES];
}

#pragma mark -  Forgot password Button Clicked
- (IBAction)btnClickedForgotPassword:(id)sender
{
    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordVCID"] animated:YES];
}


#pragma mark -  Forgot username Button Clicked
- (IBAction)btnClickedForgotUsername:(id)sender
{
    [TSMessage showNotificationInViewController:self title:@"Forgot username is coming soon." subtitle:@"" type:TSMessageNotificationTypeWarning];
}


#pragma mark - Network Delegate
-(void)didReceiveResponse:(NSDictionary *)response withUserInfo:(NSDictionary *)userInfo
{
    [Loader hideLoader];
    if ([[response objectForKey:KStatus] integerValue] == 1)
    {
    
        if ([[response objectForKey:KStatusCode] integerValue] == 1)
        {
            if ([[userInfo objectForKey:KAction] isEqualToString:KLoginAction])
            {
                NSLog(@"Log in api resulted.");
                
                [[NSUserDefaults standardUserDefaults] setObject:KStandardRegister forKey:KLoginType];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [UserDefault setUserId:[[response objectForKey:KResult] objectForKey:KUserID]];
                
                [self setRootNavigationController];
            }
            else if ([[userInfo objectForKey:KAction] isEqualToString:KSocialLoginCheckAction])
            {
                [[NSUserDefaults standardUserDefaults] setObject:KSocialRegister forKey:KLoginType];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [UserDefault setUserId:[[response objectForKey:KResult] objectForKey:KUserID]];
                [self setRootNavigationController];
            }
            
        }
        else
        {
            [TSMessage showNotificationInViewController:self title:[response objectForKey:KMsg] subtitle:@"" type:TSMessageNotificationTypeWarning];
        }
        
    }
    else if ([[response objectForKey:KStatus] integerValue] == 0)
    {
        if ([[userInfo objectForKey:KAction] isEqualToString:KSocialLoginCheckAction])
        {
            if ([userInfo objectForKey:KFBId] != nil)
            {
                [self takeUserToRegistrationScreen:socialUserDict registrationType:KFacebook];
            }
            else if ([userInfo objectForKey:KGPlusId] != nil)
            {
                [self takeUserToRegistrationScreen:socialUserDict registrationType:KGooglePlus];
                
//                [[NSUserDefaults standardUserDefaults] setObject:KSocialRegister forKey:KLoginType];
//                [[NSUserDefaults standardUserDefaults] synchronize];
//                
//                [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"HomeVCID"] animated:YES];
                
            }
            
        }
        else if ([[userInfo objectForKey:KAction] isEqualToString:KLoginAction])
        {
            [TSMessage showNotificationInViewController:self title:@"Invalid Username or Password entered." subtitle:@"" type:TSMessageNotificationTypeError];
        }


    }

    
//    [Loader hideLoader];
//    if ([[response objectForKey:kParameterMessage] length] > 0)
//    {
//        //        [Loader showSuccessMessage:[response objectForKey:kParameterMessage]];
//        [TSMessage showNotificationInViewController:self title:[response objectForKey:kParameterMessage] subtitle:@"" type:TSMessageNotificationTypeSuccess];
//    }
//    if ([[userInfo objectForKey:KActionkey]isEqualToString:kActionLogin])
//    {
//        
//        if ([[response objectForKey:kParameterStatus] integerValue] == kStatusSuccess)
//        {
//            [[NSUserDefaults standardUserDefaults] setObject:KStandardRegister forKey:KLoginType];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            
//            if ([response objectForKey:kParameterUserListResult] != nil)
//            {
//                NSDictionary * friendListDict = [response objectForKey:kParameterUserListResult];
//                [UserDefault setUserId:[friendListDict objectForKey:kUserId]]; // set user_id in userDefaults
//            }
//            [self setRootNavigationController];
//        }
//    }
//    else if ([[userInfo objectForKey:kAction] isEqualToString:KCheckSocialRegistrationAction])
//    {
//        
//        // Not Registered - need to register
//        if ([response objectForKey:KStatus] == [NSNumber numberWithInt:0])
//        {
//            //            [Loader showFailedMessage:[NSString stringWithFormat:@"%@",[response objectForKey:kParameterMessage]]];
//            [TSMessage showNotificationInViewController:self title:@"Error" subtitle:[NSString stringWithFormat:@"%@",[response objectForKey:kParameterMessage]] type:TSMessageNotificationTypeError duration:2.0];
//            
//            
//            
//            
//            if ([[userInfo objectForKey:kProvider] isEqualToString:KFacebook])
//            {
//                [self takeUserToRegistrationScreen:fbUserDict registrationType:KFacebook];
//            }
//            else if ([[userInfo objectForKey:kProvider] isEqualToString:KGooglePlus])
//            {
//                [self takeUserToRegistrationScreen:gPlusDict registrationType:KGooglePlus];
//            }
//            else if ([[userInfo objectForKey:kProvider] isEqualToString:KTwitter])
//            {
//                [self takeUserToRegistrationScreen:twitterDict registrationType:KTwitter];
//            }
//            else if ([[userInfo objectForKey:kProvider] isEqualToString:KInstagram])
//            {
//                [self takeUserToRegistrationScreen:instagramDict registrationType:KInstagram];
//            }
//            else if ([[userInfo objectForKey:kProvider] isEqualToString:KDropbox])
//            {
//                [self takeUserToRegistrationScreen:dropboxDict registrationType:KDropbox];
//            }
//            else if ([[userInfo objectForKey:kProvider] isEqualToString:KFlickr])
//            {
//                [self takeUserToRegistrationScreen:flickrDict registrationType:KFlickr];
//            }
//            
//        }
//        else
//        {
//            // Already registered - navigate into the app - home screen
//            //              [Loader showSuccessMessage:@"Already Registered!"];
//            [TSMessage showNotificationInViewController:self title:@"Already Registered!" subtitle:@"" type:TSMessageNotificationTypeSuccess duration:2.0];
//            if ([response objectForKey:kParameterUserListResult] != nil)
//            {
//                NSDictionary * userDict = [response objectForKey:kParameterUserListResult]; // need to modify - acc. to social
//                [UserDefault setUserId:[userDict objectForKey:kUserId]]; // set user_id in userDefaults
//            }
//            
//            //            MenuViewController * menuVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"MenuViewController"];
//            //
//            //            REFrostedViewController * drawerViewController = [CreateDrawer getDrawerInstance:[[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"HomeNavigationControler"] MenuViewController:menuVC];
//            //            drawerViewController.delegate = self;
//            //
//            //            //Remove the current navigation controller. - rahul's code
//            //            [self.navigationController.view removeFromSuperview];
//            //            [AppDelegate getWindowInstance].rootViewController = drawerViewController;
//            [self setRootNavigationController];
//        }
//        
//    }
//    else if ([[userInfo objectForKey:KActionkey] isEqualToString:KGetBucketUrlAction])
//    {
//        [network setBucketURL:[[response objectForKey:kParameterUserListResult] objectForKey:KBucketURL]];
//        [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:kParameterUserListResult] objectForKey:KBucketName] forKey:KBucketName];
//    }
//    
    
}


-(void)setRootNavigationController
{
    MMDrawerController * drawerController;
    
    MenuVC * menuVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"MenuVCID"];
    

    UIViewController * centerViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"HomeVCID"];
    
    UIViewController * rightSideDrawerViewController = menuVC;
    
    UINavigationController * navigationController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"HomeNavigationControllerID"];
    [AppDelegate appDelegate].homeNavController = navigationController;
    [navigationController setRestorationIdentifier:@"MMExampleCenterNavigationControllerRestorationKey"];
    
    if(SYSTEM_VERSION_GREATER_THAN(@"6.0"))
    {
        UINavigationController * rightSideNavController = [[UINavigationController alloc] initWithRootViewController:rightSideDrawerViewController];
        [rightSideNavController setRestorationIdentifier:@"MMExampleRightNavigationControllerRestorationKey"];

        drawerController = [[MMDrawerController alloc]
                            initWithCenterViewController:navigationController
                            leftDrawerViewController:nil
                            rightDrawerViewController:rightSideNavController];
        [drawerController setShowsShadow:NO];
    }
    else{
        drawerController = [[MMDrawerController alloc]
                            initWithCenterViewController:navigationController
                            leftDrawerViewController:nil
                            rightDrawerViewController:rightSideDrawerViewController];
    }
    [drawerController setRestorationIdentifier:@"MMDrawer"];
    [drawerController setMaximumRightDrawerWidth:centerViewController.view.bounds.size.width - 100];
    [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    [drawerController
     setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
         MMDrawerControllerDrawerVisualStateBlock block;
         block = [[MMExampleDrawerVisualStateManager sharedManager]
                  drawerVisualStateBlockForDrawerSide:drawerSide];
         if(block){
             block(drawerController, drawerSide, percentVisible);
         }
     }];
    
    UIWindow * window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    if(SYSTEM_VERSION_GREATER_THAN(@"6.0"))
    {
        UIColor * tintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navBarStrip.jpg"]];
        [window setTintColor:tintColor];
    }
    
    [AppDelegate appDelegate].drawerController = drawerController;
    
    //Remove the current navigation controller. - rahul's code
//    [self.navigationController.view removeFromSuperview];
    
    [AppDelegate getWindowInstance].rootViewController = drawerController;
    [[AppDelegate getWindowInstance] makeKeyAndVisible];
}


- (void)didFailWithError:(NSError *)error withUserInfo:(NSDictionary *)userInfo
{
    
    [Loader hideLoader];
    if (error.code == 3840)
    {
        [TSMessage showNotificationInViewController:self title:@"Error" subtitle:@"Sorry! Something went wrong!" type:TSMessageNotificationTypeError duration:2.0];
    }
    else if (error.code == -1004)
    {
        UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"No Internet Connection" message:@"Please check your internet connection." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Settings", nil];
        alertView.tag = 1002;
        [alertView show];
    }
    else
    {
        [TSMessage showNotificationInViewController:self title:@"Error" subtitle:error.localizedDescription type:TSMessageNotificationTypeError duration:2.0];
        
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        if (alertView.tag == 1002)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }
    else
    {
        [Loader hideLoader];
    }
}

#pragma mark
#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField       // return NO to disallow editing.
{
    
    return YES;
    
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField  // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
{
    [textField resignFirstResponder];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField             // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
{
    
    [textField resignFirstResponder];
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string   // return NO to not change text
{
    //    if (textField.tag != 2)
    //    {
    //        textField.text = [textField.text capitalizedString];
    //    }
    return YES;
    
}


- (BOOL)textFieldShouldClear:(UITextField *)textField               // called when clear button pressed. return NO to ignore (no notifications)
{
    
    [textField resignFirstResponder];
    return YES;
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField.tag != 2)
    {
        LoginCell *cell = (LoginCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        UIView * tf = [cell viewWithTag:textField.tag + 1];
        [tf becomeFirstResponder];
    }
    else if (textField.tag == 2)
    {
        if (textField.returnKeyType == UIReturnKeyGo)
        {
            [self loginButtonClicked:nil];
        }
    }
    return YES;
    
}


#pragma M<ark - Register Button Clicked
- (IBAction)btnClickedRegisterYourself:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setValue:KStandardRegister forKey:KLoginType];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController pushViewController: [self.storyboard instantiateViewControllerWithIdentifier:@"RegistrationVCID"] animated:YES];
}

#pragma mark - Login Button Clicked
- (IBAction)loginButtonClicked:(id)sender
{
    [self.view endEditing:YES]; // hide keyboard and end editing textfield
    
    if ([[self getLoginCellInstance].textFieldUsername.text length] > 0 && ![[self getLoginCellInstance].textFieldUsername.text isEqualToString:@" "])
    {
        if ([[self getLoginCellInstance].textFieldPassword.text length] > 0 && ![[self getLoginCellInstance].textFieldPassword.text isEqualToString:@" "])
        {
            // login button clicked
            LoginReq * loginReq  = [[LoginReq alloc] init];
            loginReq.action  = KLoginAction;
            loginReq.user_name = [self getLoginCellInstance].textFieldUsername.text;
            loginReq.user_password = [self getLoginCellInstance].textFieldPassword.text;
            
            [network userLogin:loginReq UserInfo:[[NSDictionary alloc] initWithObjectsAndKeys:KLoginAction,KAction, nil]];
        }
        else
        {
            [TSMessage showNotificationInViewController:self title:@"Invalid password entered!" subtitle:@"" type:TSMessageNotificationTypeError];
        }
    }
    else
    {
        [TSMessage showNotificationInViewController:self title:@"Invalid Username entered!" subtitle:@"" type:TSMessageNotificationTypeError];
    }
}


#pragma mark - Get Login Cell Instance Method
-(LoginCell *) getLoginCellInstance
{
    return (LoginCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

#pragma Mark - Forgot Password
- (IBAction)forgotPasswordBtnClicked:(id)sender
{
    
    // iPad
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
//        UIPopoverController * popover = [[UIPopoverController alloc] initWithContentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordVCID"]];
//        popover.delegate = self;
//        
//        [popover presentPopoverFromBarButtonItem:sender
//                        permittedArrowDirections:UIPopoverArrowDirectionAny
//                                        animated:YES];
    }
    else
    {
//        ForgotPasswordVC * instance = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordVCID"];
//        [self presentViewController:instance animated:YES completion:nil];
    }
    
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma mark - Facebook Login
- (IBAction)facebookBtnClicked:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:KSocialRegister forKey:KLoginType];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:KFacebook forKey:KSocialType];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self loginWithFaceBook];
    
}


-(void)loginWithFaceBook
{
    NSArray *Permissions = @[@"public_profile", @"email"];
    
    [FBSession openActiveSessionWithPublishPermissions:Permissions defaultAudience:FBSessionDefaultAudienceFriends allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
        //        [session closeAndClearTokenInformation];
        NSLog(@"done");
        //        NSLog(@"token : %@",session.accessTokenData.accessToken);
        if(!session.accessTokenData.accessToken){
            //            _fbBlock(nil);
            return ;
        }
        
        switch (status) {
            case FBSessionStateClosed:
                //need to handle
                break;
            case FBSessionStateCreated:
                break;
            case  FBSessionStateCreatedOpening:
                break;
            case FBSessionStateClosedLoginFailed:
                [FBSession.activeSession closeAndClearTokenInformation];
                //need to handle
                break;
            case FBSessionStateOpen:
                [[FBRequest requestForMe] startWithCompletionHandler:
                 ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
                     
                     NSLog(@"user :%@",user); // here got the user Info.
                     
                     [[NSUserDefaults standardUserDefaults] setObject:KSocialRegister forKey:KLoginType];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     [[NSUserDefaults standardUserDefaults] setObject:KFacebook forKey:KSocialType];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     
                     socialUserDict = [NSMutableDictionary dictionaryWithDictionary:user];
                     [socialUserDict setObject:KSocialType forKey:KFacebook];
                     
                     if (![[user objectForKey:@"id"] isEqual:[NSNull null]] || [user objectForKey:@"id"] != nil)
                     {
                         [Loader showLoader];
                         [self checkForSocialLogin:[user objectForKey:@"id"] Provider:KFacebook];
                     }
                     else
                     {
                         //                                              [Loader showFailedMessage:@"There is Some problem!"];
                         
                         [TSMessage showNotificationInViewController:self title:@"Error" subtitle:@"Sorry! Something went wrong!" type:TSMessageNotificationTypeError duration:2.0];
                     }

                 }];
                break;
                
        }
        
    }];
}


-(void)dataForFacebook
{

}

-(void)uploadToFacebook
{
    [self dataForFacebook];
    
    
    if ([FBSession.activeSession.permissions
         indexOfObject:@"publish_actions"] == NSNotFound) {
        
        NSArray *permissions =
        [NSArray arrayWithObjects:@"publish_actions",@"publish_stream", nil];
        [[FBSession activeSession] requestNewPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceFriends
                                              completionHandler:^(FBSession *session, NSError *error) {}];
    }
    else
    {
        NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
        [params setObject:@"Photo post test..." forKey:@"message"];
        [params setObject:[UIImage imageNamed:@"Avatar2.png"] forKey:@"picture"];
        //for not allowing multiple hits
        
        [FBRequestConnection startWithGraphPath:@"me/photos"
                                     parameters:params
                                     HTTPMethod:@"POST"
                              completionHandler:^(FBRequestConnection *connection,
                                                  id result,
                                                  NSError *error)
         {
             if (error)
             {
                 //showing an alert for failure
                 UIAlertView *alertView = [[UIAlertView alloc]
                                           initWithTitle:@"Post Failed"
                                           message:error.localizedDescription
                                           delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
                 [alertView show];
                 
             }
             else
             {
                 //showing an alert for success
                 UIAlertView *alertView = [[UIAlertView alloc]
                                           initWithTitle:@"Post success"
                                           message:@"Shared the photo successfully"
                                           delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
                 [alertView show];
                 
             }
         }];
    }
    
}

#pragma mark - GooglePlus

- (IBAction)gPlusBtnClicked:(id)sender
{
        [[NSUserDefaults standardUserDefaults] setObject:KYes forKey:KGooglePlus];
        [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self setUpGooglePlus];
}


-(void) setUpGooglePlus
{
    LoginCell *cell = (LoginCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    cell.btnOutletGooglePlus.style = kGPPSignInButtonStyleStandard;
//    [cell.btnOutletGooglePlus setColorScheme:kGPPSignInButtonColorSchemeDark];
    
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.delegate = self;
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;
    
    //    [signIn authenticate];
    // You previously set kClientId in the "Initialize the Google+ client" step
    signIn.clientID = @"1040905472018-3nk8aa2aig9nepgk3j1v7uc8qoqtmcao.apps.googleusercontent.com";
    
    
    [GPPSignIn sharedInstance].actions = [NSArray arrayWithObjects:
                                          @"http://schemas.google.com/AddActivity",
                                          @"http://schemas.google.com/BuyActivity",
                                          @"http://schemas.google.com/CheckInActivity",
                                          @"http://schemas.google.com/CommentActivity",
                                          @"http://schemas.google.com/CreateActivity",
                                          @"http://schemas.google.com/ListenActivity",
                                          @"http://schemas.google.com/ReserveActivity",
                                          @"http://schemas.google.com/ReviewActivity",
                                          nil];
    
    
    
    // Uncomment one of these two statements for the scope you chose in the previous step
    signIn.scopes = @[ kGTLAuthScopePlusLogin];  // "https://www.googleapis.com/auth/plus.login" scope
    //    signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = self;
    
    [[NSUserDefaults standardUserDefaults] setObject:KSocialRegister forKey:KLoginType];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:KGooglePlus forKey:KSocialType];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}


- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth  error: (NSError *) error
{
    
    NSLog(@"Received error %@ and auth object %@",error, auth);
    
    if (error) {
        return;
    }
    // getting the access token from auth
    NSString  *accessTocken = [auth valueForKey:@"accessToken"]; // access tocken pass in .pch file
    NSString *str=[NSString stringWithFormat:GPPUserInfoURL,accessTocken];
    NSString *escapedUrl = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",escapedUrl]];
    NSString *jsonData = [[NSString alloc] initWithContentsOfURL:url usedEncoding:nil error:nil];
    
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:[jsonData dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error]; // user info
    
    socialUserDict = [NSMutableDictionary dictionaryWithDictionary:jsonDictionary];
    [socialUserDict setObject:KSocialType forKey:KGooglePlus];
    

//
    if (![[socialUserDict objectForKey:@"id"] isEqual:[NSNull null]] || [socialUserDict objectForKey:@"id"] != nil)
    {
        [[NSUserDefaults standardUserDefaults] setObject:KSocialRegister forKey:KLoginType];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setObject:KGooglePlus forKey:KSocialType];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self checkForSocialLogin:[socialUserDict objectForKey:@"id"] Provider:KGooglePlus];
    }
    else
    {
        [TSMessage showNotificationInViewController:self title:@"Error" subtitle:@"Sorry! Something went wrong!" type:TSMessageNotificationTypeError duration:2.0];
   
    }
    
}

- (void)finishedSharing:(BOOL)shared // Gplus Social Sharing
{
    if (shared)
    {
        NSLog(@"User successfully shared!");
    }
    else
    {
        NSLog(@"User didn't share.");
    }
}

- (void)signOut
{
    [[GPPSignIn sharedInstance] signOut];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
