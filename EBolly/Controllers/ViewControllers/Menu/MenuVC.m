//
//  MenuVC.m
//  EBolly
//
//  Created by Yashvir on 10/08/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import "MenuVC.h"
#import "HomeNavigationController.h"
#import "AppDelegate.h"
#import "UserDefault.h"
#import "Network.h"
#import "TSMessage.h"

#import "MenuCell.h"
#import "HomeVC.h"
#import "WallpaperVC.h"
#import "WallpaperVC.h"


@interface MenuVC ()
{
    NSArray *dataArray;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MenuVC

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initializeVariables];
    [self viewSetUp];
}


#pragma mark - SetUpView
- (void) initializeVariables
{
    dataArray = [NSArray arrayWithObjects:@"Home",@"Wallpaper",@"News",@"Contact Us",@"Invite a friend",@"Logout", nil];
}


- (void) viewSetUp
{
    self.tableView.separatorColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:0.35f];
//    self.tableView.backgroundColor = [UIColor lightGrayColor];

    
    if(SYSTEM_VERSION_GREATER_THAN(@"6.0"))
    {
        UIColor * barColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navBarStrip.jpg"]];
        [self.navigationController.navigationBar setBarTintColor:barColor];
    }
    else
    {
        UIColor * barColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navBarStrip.jpg"]];
        [self.navigationController.navigationBar setTintColor:barColor];
    }
    [self.view setBackgroundColor:[UIColor darkGrayColor]];
}


#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.textLabel.textColor = [UIColor whiteColor];

}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 1)];
    view.backgroundColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:0.6f];
    
    return view;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.5;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)sectionIndex
{
    return 0.0;
}



#pragma mark - orientation
-(void) restrictRotation:(BOOL) restriction
{
    AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.restrictRotation = restriction;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [[AppDelegate appDelegate].drawerController closeDrawerAnimated:YES completion:nil];
    [[AppDelegate appDelegate].homeNavController setNavigationBarHidden:NO];
    
    NSNumber *value = [NSNumber numberWithInt:UIDeviceOrientationPortrait];// set orientation portrait
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];// set orientation portrait
    [self restrictRotation:YES];// set orientation
    
    if (indexPath.section == 0 && indexPath.row == 0)
    {
        [[AppDelegate appDelegate].homeNavController popToRootViewControllerAnimated:YES];
    }
    else if (indexPath.section == 1 && indexPath.row == 0)
    {
        // wallpaper
        [[AppDelegate appDelegate].homeNavController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"WallpaperVCID"] animated:YES];
    }
    else if (indexPath.section == 2 && indexPath.row == 0)
    {
        // NEWS
    }
    else if (indexPath.section == 3 && indexPath.row == 0)
    {
        // CONTACT US
    }
    else if (indexPath.section == 4 && indexPath.row == 0)
    {
        // INVITE A FRIEND
    }
    else if (indexPath.section == 5 && indexPath.row == 0)
    {
        // Logout
        [UserDefault removeAllUserDefaults]; // deleting all the user credentials - from userDefaults
        [[AppDelegate appDelegate] getRootNavigationController]; // taking user back to login screen.
    }
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 6;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"Cell";
    
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell.textLabel sizeToFit];
//    [cell setBackgroundColor:[UIColor lightGrayColor]];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor darkGrayColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    if (indexPath.section == 0 && indexPath.row == 0)
    {
        // move to home screen
        cell.textLabel.text = dataArray[indexPath.section];
        cell.imageView.image = [UIImage imageNamed:@"home_48x48.png"];

    }
    else if (indexPath.section == 1 && indexPath.row == 0)
    {
        // Wallpaper
        cell.textLabel.text = dataArray[indexPath.section];
        cell.imageView.image = [UIImage imageNamed:@"home_48x48.png"];
        
    }
    else if (indexPath.section == 2 && indexPath.row == 0)
    {
        //News
        cell.textLabel.text = dataArray[indexPath.section];
        cell.imageView.image = [UIImage imageNamed:@"home_48x48.png"];

    }
    else if (indexPath.section == 3 && indexPath.row == 0)
    {
        // Contact Us
        cell.textLabel.text = dataArray[indexPath.section];
        cell.imageView.image = [UIImage imageNamed:@"home_48x48.png"];
    }
    else if (indexPath.section == 4 && indexPath.row == 0)
    {
        // Invite a Friend
        cell.textLabel.text = dataArray[indexPath.section];
        cell.imageView.image = [UIImage imageNamed:@"home_48x48.png"];

    }
    else if (indexPath.section == 5 && indexPath.row == 0)
    {
        // Invite a Friend
        cell.textLabel.text = dataArray[indexPath.section];
        cell.imageView.image = [UIImage imageNamed:@"home_48x48.png"];
        
    }
    return cell;
}


#pragma mark - Memory Management
-(void)dealloc
{
    _tableView = nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
