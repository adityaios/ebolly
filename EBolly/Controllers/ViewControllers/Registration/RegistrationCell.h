//
//  RegistrationCell.h
//  EBolly
//
//  Created by Yashvir on 31/07/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *textFieldIAmA;
@property (weak, nonatomic) IBOutlet UITextField *textFieldFIrstName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldLastName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldUsername;
@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPhNumber;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (weak, nonatomic) IBOutlet UITextField *textFieldVerifyPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnOutletRegister;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewProfileImage;
@property (weak, nonatomic) IBOutlet UITextField *textFieldGender;

@end
