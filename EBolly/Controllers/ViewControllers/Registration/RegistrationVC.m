//
//  RegistrationVC.m
//  EBolly
//
//  Created by Yashvir on 31/07/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import "RegistrationVC.h"
#import "RegistrationCell.h"
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <FacebookSDK/FacebookSDK.h>
#import "Loader.h"
#import "Network.h"
#import "AppDelegate.h"
#import "RegisterReq.h"
#import "TSMessage.h"
#import "BasicUtilities.h"
#import "RegisterReq.h"
#import "UserDefault.h"

#import "MMDrawerController.h" // drawer
#import "MMDrawerVisualState.h" // drawer
#import "MMExampleDrawerVisualStateManager.h" // drawer
#import "MMNavigationController.h" // drawer

#import "MenuVC.h"

static int const KTextFieldImA = 1;
static int const KTextFieldGender = 9;
static int const KPickerViewImA = 20;
static int const KPickerViewGender = 21;

@interface RegistrationVC ()<NetworkDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    NSDictionary * fbUserDict; // Facebook user Details Dict
    Network * network;
    float keyboardFrameHeight;
    UIImage * selectedAvatarImage;
    NSMutableArray * genderArr;
    NSMutableArray * iMTypeArr;
    NSString * pickerValue;
}

@property (weak, nonatomic) IBOutlet UIPickerView *pickerViewIMType;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbarViewSelectType;
@property (weak, nonatomic) IBOutlet UIView *viewBasePickerView;

@end

@implementation RegistrationVC

#pragma mark - View Life Cycle


#pragma mark - Tableview Datasource Methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.view.frame.size.height;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RegistrationCell * cell = (RegistrationCell *)[_tableView dequeueReusableCellWithIdentifier:@"Cell"];
    return cell;
}




#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [self initializeVariables];
    [self viewSetUp];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    
    
    
    [self registerForKeyboardNotifications];
    
    
}




-(void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:KLoginType] isEqualToString:KSocialRegister])
    {
        [self updateView];
    }
}



// called in case of social login
-(void)updateView
{
    
    RegistrationCell * cell = (RegistrationCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:KSocialType] isEqualToString:KFacebook])
    {
        cell.textFieldFIrstName.text = ([_userDetailDict objectForKey:@"name"] != [NSNull null] )? [_userDetailDict objectForKey:@"name"]: @"";
        //        cell.textFieldLastName.text = ([_userDetailDict objectForKey:@"last_name"] != [NSNull null]) ? [_userDetailDict objectForKey:@"last_name"] : @"";
        
        if ([_userDetailDict objectForKey:@"email"] != [NSNull null] && [_userDetailDict objectForKey:@"email"])
        {
            cell.textFieldEmail.text = [NSString stringWithFormat:@"%@",[_userDetailDict objectForKey:@"email"]];
        }
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:KSocialType] isEqualToString:KGooglePlus])
    {
        //        cell.textFieldFullName.text = [NSString stringWithFormat:@"%@",[_userDetailDict objectForKey:@"name"]];
        //        cell.textFieldFIrstName.text = ([_userDetailDict objectForKey:@"name"] != [NSNull null] )? [_userDetailDict objectForKey:@"name"]: @"";
        cell.textFieldFIrstName.text = ([_userDetailDict objectForKey:@"given_name"] != [NSNull null] )? [_userDetailDict objectForKey:@"given_name"]: @"";
        cell.textFieldLastName.text = ([_userDetailDict objectForKey:@"family_name"] != [NSNull null] )? [_userDetailDict objectForKey:@"family_name"]: @"";
        
        
        if ([_userDetailDict objectForKey:@"email"] != [NSNull null] && [_userDetailDict objectForKey:@"email"])
        {
            cell.textFieldEmail.text = [NSString stringWithFormat:@"%@",[_userDetailDict objectForKey:@"email"]];
        }
    }
}


#pragma mark - SetUpView
- (void) initializeVariables
{
    network = [[Network alloc] init];
    network.delegate = self;
    
    genderArr = [[NSMutableArray alloc] initWithObjects:@"Male",@"Female", nil];
    iMTypeArr = [[NSMutableArray alloc] initWithObjects:@"Talent",@"Producer",@"Service Provider", nil];
}


- (void) viewSetUp
{
    UIScreenEdgePanGestureRecognizer * screenEdgePanGesture = [[UIScreenEdgePanGestureRecognizer alloc]initWithTarget:self action:@selector(screenEdgeGestureReceived:)];
    
    screenEdgePanGesture.edges = UIRectEdgeLeft;
    
    screenEdgePanGesture.enabled = YES;
    
    [self.view addGestureRecognizer:screenEdgePanGesture];
    
    
    [self setNavigationBar];
    
    // setting background image
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"dashboard_bg1.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    
    [self getRegistrationCellInstance].textFieldIAmA.inputView = _viewBasePickerView;
    
    [self callGetUserTypeWS];
}

-(RegistrationCell * )getRegistrationCellInstance
{
    return (RegistrationCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

#pragma mark - Picker View Toolbar Done button Clicked
- (IBAction)btnClickedSelectType:(id)sender
{
    NSLog(@"DONE");
    _viewBasePickerView.hidden = YES;
    if (_pickerViewIMType.tag == KPickerViewImA)
    {
        [self getRegistrationCellInstance].textFieldIAmA.text = pickerValue;
    }
    else if (_pickerViewIMType.tag == KPickerViewGender)
    {
        [self getRegistrationCellInstance].textFieldGender.text = pickerValue;
    }
}


-(void)screenEdgeGestureReceived:(UIGestureRecognizer *)edgeGesture
{
    [self.navigationController popViewControllerAnimated:YES];
}



-(void)setNavigationBar
{
    
    self.title = NSLocalizedString(@"Registration", @"Registration");
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]}; //set color of navigation bar title color.
    //    self.navigationItem.backBarButtonItem.title = @"Back";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    //    [self setUpMenuButton];
    [self.navigationItem setHidesBackButton:YES animated:YES];
    //    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    [self setNavigationLeftBarBtn];
}

-(void) setNavigationLeftBarBtn
{
    //    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    
    UIButton * backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 50, 20)];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundColor:[UIColor clearColor]];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"OpenSans-CondensedBold" size:15.0]];
    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    
    UIBarButtonItem * backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    
    NSArray * leftBarButtonsArray = [[NSArray alloc]initWithObjects:backButtonItem, nil];
    self.navigationItem.leftBarButtonItems = leftBarButtonsArray;
    
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(RegistrationCell *) getRegistrationCell
{
    return (RegistrationCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}


#pragma mark - Button Action
- (IBAction)btnClickedRegister:(id)sender
{
    if ([self getRegistrationCellInstance].textFieldIAmA.text && [[self getRegistrationCellInstance].textFieldIAmA.text length] > 0 && ![[self getRegistrationCellInstance].textFieldIAmA.text isEqualToString:@" "])
    {
        if ([self getRegistrationCellInstance].textFieldFIrstName.text && [[self getRegistrationCellInstance].textFieldFIrstName.text length] > 0 && ![[self getRegistrationCellInstance].textFieldFIrstName.text isEqualToString:@" "])
        {
            if ([self getRegistrationCellInstance].textFieldLastName.text && [[self getRegistrationCellInstance].textFieldLastName.text length] > 0 && ![[self getRegistrationCellInstance].textFieldLastName.text isEqualToString:@" "])
            {
                if ([self getRegistrationCellInstance].textFieldUsername.text && [[self getRegistrationCellInstance].textFieldUsername.text length] > 0 && ![[self getRegistrationCellInstance].textFieldUsername.text isEqualToString:@" "])
                {
                    if ([self getRegistrationCellInstance].textFieldEmail.text && [[self getRegistrationCellInstance].textFieldEmail.text length] > 0 && ![[self getRegistrationCellInstance].textFieldEmail.text isEqualToString:@" "])
                    {
                        if ([BasicUtilities validateEmailWithString:[self getRegistrationCellInstance].textFieldEmail.text])
                        {
                            if ([self getRegistrationCellInstance].textFieldPhNumber.text && [[self getRegistrationCellInstance].textFieldPhNumber.text length] > 0 && ![[self getRegistrationCellInstance].textFieldPhNumber.text isEqualToString:@" "])
                            {
                                if ([BasicUtilities validateMobileNumber:[self getRegistrationCellInstance].textFieldPhNumber.text])
                                {
                                    if ([self getRegistrationCellInstance].textFieldGender.text && [[self getRegistrationCellInstance].textFieldGender.text length] > 0 && ![[self getRegistrationCellInstance].textFieldGender.text isEqualToString:@" "])
                                    {
                                        if ([self getRegistrationCellInstance].textFieldPassword.text && [[self getRegistrationCellInstance].textFieldPassword.text length] >= 6 && ![[self getRegistrationCellInstance].textFieldPassword.text isEqualToString:@" "] &&  [[self getRegistrationCellInstance].textFieldPassword.text length] <= 14)
                                        {
                                            
                                            if ([self getRegistrationCellInstance].textFieldVerifyPassword.text && [[self getRegistrationCellInstance].textFieldVerifyPassword.text length] >= 6 && ![[self getRegistrationCellInstance].textFieldVerifyPassword.text isEqualToString:@" "] &&  [[self getRegistrationCellInstance].textFieldVerifyPassword.text length] <= 14  &&  [[self getRegistrationCellInstance].textFieldVerifyPassword.text isEqualToString:[self getRegistrationCellInstance].textFieldPassword.text])
                                            {
                                                // Successfully Validated - CALL WS
                                                
                                                RegisterReq * req = [[RegisterReq alloc] init];
                                                req.firstName = [self getRegistrationCellInstance].textFieldFIrstName.text;
                                                req.lastName = [self getRegistrationCellInstance].textFieldLastName.text;
                                                req.username = [self getRegistrationCellInstance].textFieldUsername.text;
                                                req.emailId = [self getRegistrationCellInstance].textFieldEmail.text;
                                                req.phNumber = [self getRegistrationCellInstance].textFieldPhNumber.text;


                                                if ([[self getRegistrationCellInstance].textFieldGender.text isEqualToString:@"Male"])
                                                {
                                                    req.gender = KGenderMale;
                                                }
                                                else if ([[self getRegistrationCellInstance].textFieldGender.text isEqualToString:@"Female"])
                                                {
                                                    req.gender = KGenderFemale;
                                                }
                                                else
                                                {
                                                    [TSMessage showNotificationInViewController:self title:@"Please select gender." subtitle:@"" type:TSMessageNotificationTypeError];
                                                }
                                                
                                                req.userType = [self getRegistrationCellInstance].textFieldIAmA.text;
                                                req.fbId = [_userDetailDict objectForKey:KFBId];
                                                req.gPlusId = [_userDetailDict objectForKey:@"id"];
                                                req.password = [self getRegistrationCellInstance].textFieldPassword.text;
                                                [network userRegister:req UserInfo:[[NSDictionary alloc] initWithObjectsAndKeys:KRegistrationAction,KAction, nil]];
                                            }
                                            else
                                            {
                                                [TSMessage showNotificationInViewController:self title:@"Password doesn't match." subtitle:@"Try Again!" type:TSMessageNotificationTypeError];
                                            }
                                            
                                        }
                                        else
                                        {
                                            [TSMessage showNotificationInViewController:self title:@"Password must be alteast 6 characters and can be atmost 14 character long." subtitle:@"Try Again!" type:TSMessageNotificationTypeError];
                                        }
                                        
                                    }
                                    else
                                    {
                                        [TSMessage showNotificationInViewController:self title:@"Please select gender." subtitle:@"Try Again!" type:TSMessageNotificationTypeError];
                                    }
                                }
                                else
                                {
                                    [TSMessage showNotificationInViewController:self title:@"Invalid mobile number entered." subtitle:@"" type:TSMessageNotificationTypeError];
                                }
                                
                                
                                
                            }
                            else
                            {
                                [TSMessage showNotificationInViewController:self title:@"Please enter your mobile number." subtitle:@"Try Again!" type:TSMessageNotificationTypeError];
                            }
                            
                        }
                        else
                        {
                            [TSMessage showNotificationInViewController:self title:@"Invalid emailId entered." subtitle:@"Try Again!" type:TSMessageNotificationTypeError];
                        }
                    }
                    else
                    {
                        [TSMessage showNotificationInViewController:self title:@"Please enter your emailId." subtitle:@"Try Again!" type:TSMessageNotificationTypeError];
                    }
                    
                }
                else
                {
                    [TSMessage showNotificationInViewController:self title:@"Please enter your user name." subtitle:@"Try Again!" type:TSMessageNotificationTypeError];
                }
            }
            else
            {
                [TSMessage showNotificationInViewController:self title:@"Please enter your last name." subtitle:@"Try Again!" type:TSMessageNotificationTypeError];
            }
            
        }
        else
        {
            [TSMessage showNotificationInViewController:self title:@"Please enter your first name." subtitle:@"Try Again!" type:TSMessageNotificationTypeError];
        }
        
    }
    else
    {
        [TSMessage showNotificationInViewController:self title:@"Please select user type." subtitle:@"Try Again!" type:TSMessageNotificationTypeError];
    }
    
    
    
}

- (void) registerButtonAction
{
    
    //
    //    NSString *fullname = [[self getRegistrationCell].textFieldFullName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //    NSString *email = [[self getRegistrationCell].textFieldEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //    NSString *username = [[self getRegistrationCell].textFieldUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //    NSString *password = [[self getRegistrationCell].textFieldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //
    //
    //
    //    else
    //    {
    //        [Loader showLoader];
    //
    //        if ([[[NSUserDefaults standardUserDefaults] objectForKey:kProvider] isEqualToString:KFacebook])
    //        {
    //            [network userRegistration:[NSDictionary dictionaryWithObjectsAndKeys:kActionRegistration,kAction,username,kParameterUsername,password,kParameterPassword,firstname,kParameterFullname,email,kParameterEmail,[_userDetailDict objectForKey:@"id"],KFBId,nil]];
    //        }
    //        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:kProvider] isEqualToString:KGooglePlus])
    //        {
    //            [network userRegistration:[NSDictionary dictionaryWithObjectsAndKeys:kActionRegistration,kAction,username,kParameterUsername,password,kParameterPassword,firstname,kParameterFullname,email,kParameterEmail,[_userDetailDict objectForKey:@"id"],kGplusId,nil]];
    //        }
    //        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:kProvider] isEqualToString:KTwitter])
    //        {
    //            [network userRegistration:[NSDictionary dictionaryWithObjectsAndKeys:kActionRegistration,kAction,username,kParameterUsername,password,kParameterPassword,firstname,kParameterFullname,email,kParameterEmail,[_userDetailDict objectForKey:@"id"],KTwitterId,nil]];
    //        }
    //        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:kProvider] isEqualToString:KFlickr])
    //        {
    //            [network userRegistration:[NSDictionary dictionaryWithObjectsAndKeys:kActionRegistration,kAction,username,kParameterUsername,password,kParameterPassword,firstname,kParameterFullname,email,kParameterEmail,[_userDetailDict objectForKey:@"id"],KFlickrId,nil]];
    //        }
    //        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:kProvider] isEqualToString:KInstagram])
    //        {
    //            [network userRegistration:[NSDictionary dictionaryWithObjectsAndKeys:kActionRegistration,kAction,username,kParameterUsername,password,kParameterPassword,firstname,kParameterFullname,email,kParameterEmail,[_userDetailDict objectForKey:@"id"],KSocialconfigInstagram,nil]];
    //        }
    //        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:kProvider] isEqualToString:KDropbox])
    //        {
    //            [network userRegistration:[NSDictionary dictionaryWithObjectsAndKeys:kActionRegistration,kAction,username,kParameterUsername,password,kParameterPassword,firstname,kParameterFullname,email,kParameterEmail,[_userDetailDict objectForKey:@"id"],KSocialconfigDropbox,nil]];
    //        }
    //        else
    //        {
    //            [network userRegistration:[NSDictionary dictionaryWithObjectsAndKeys:kActionRegistration,kAction,username,kParameterUsername,password,kParameterPassword,firstname,kParameterFullname,email,kParameterEmail,nil]];
    //        }
    //
    //    }
    //
}


#pragma mark - Keyboard Notifications
- (void)registerForKeyboardNotifications
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    
}


- (void)removeKeyboardNotifications
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}



- (void)keyboardWasShown:(NSNotification*)aNotification
{
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonClicked)];
    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
    
    NSDictionary *userInfo = [aNotification userInfo];
    CGRect keyboardEndFrame;
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
    UIEdgeInsets insets = self.tableView.contentInset;
    insets.bottom = keyboardFrame.size.height;
    self.tableView.contentInset = insets;
    
}


- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    UIEdgeInsets insets = self.tableView.contentInset;
    insets.bottom = 0;
    self.tableView.contentInset = insets;
    
}

-(void)doneButtonClicked
{
    //    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:informationBtn];
    [self.view endEditing:YES];
}

#pragma mark -
- (IBAction)btnClickedAddProfileImage:(id)sender
{
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Select image from" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"From Camera",@"From Library", nil];
    
    [action showInView:self.view];
    
}

#pragma mark - Get User Type From WS
-(void)callGetUserTypeWS
{
    [network getUserType];
}


#pragma mark - ActionSheet delegates

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    UIImagePickerController *pickerView =[[UIImagePickerController alloc]init];
    pickerView.allowsEditing = NO;
    pickerView.delegate = self;
    [pickerView.navigationBar setTintColor:[UIColor whiteColor]];
    [pickerView.navigationBar setBackgroundImage:[[UIImage imageNamed:@"header.jpg"]                                                                                                 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch] forBarMetrics:UIBarMetricsDefault];
    [pickerView.navigationBar setTintColor:[UIColor whiteColor]];
    //    [pickerView.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    
    if( buttonIndex == 0 )
    {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            pickerView.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:pickerView animated:NO completion:nil];
        }
        else
        {
            //            [Loader showFailedMessage:@"Camera Not Found On Device!"];
            //            [TSMessage showNotificationInViewController:self title:@"Error" subtitle:@"Camera not found." type:TSMessageNotificationTypeError duration:2.0];
            
            
        }
    }
    else if( buttonIndex == 1 )
    {
        [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [self presentViewController:pickerView animated:NO completion:nil];
    }
    
}


#pragma mark - PickerDelegates

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    selectedAvatarImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    if([info objectForKey:@"UIImagePickerControllerMediaMetadata"] != nil)
    {
        UIImageWriteToSavedPhotosAlbum(selectedAvatarImage, nil, nil, nil); // save image to camera roll
    }
    
    [self getRegistrationCell].imgViewProfileImage.image = selectedAvatarImage;
    //    ((ProfileCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]).profileImageView.image = _selectedAvatarImage ;
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark - Network Delegate
-(void)didReceiveResponse:(NSDictionary *)response withUserInfo:(NSDictionary *)userInfo
{
    @try
    {
    
    [Loader hideLoader];
    if ([[response objectForKey:KStatus] integerValue] == 1)
    {
        
        if ([[response  objectForKey:KStatusCode] integerValue] == 1)
        {
            if ([[userInfo objectForKey:KAction] isEqualToString:KGetUserTypeAction])
            {
                if ([NSMutableArray arrayWithArray:[[response objectForKey:KResult] objectForKey:KUserType]] != nil)
                {
                    iMTypeArr = [NSMutableArray arrayWithArray:[[response objectForKey:KResult] objectForKey:KUserType]];
                }
            }
            else if ([[userInfo objectForKey:KAction] isEqualToString:KRegistrationAction])
            {
                [TSMessage showNotificationInViewController:self title:[response objectForKey:KMsg] subtitle:@"" type:TSMessageNotificationTypeSuccess];
                [UserDefault setUserId:[[[[response objectForKey:KResult] objectForKey:KUserDetails] objectAtIndex:0] objectForKey:KUserID]];
                
                [self setRootNavigationController];
            }
        }
        else
        {
            [TSMessage showNotificationInViewController:self title:[response objectForKey:KMsg] subtitle:@"" type:TSMessageNotificationTypeWarning];
        }
        
    }
    else if ([[response objectForKey:KStatus] integerValue] == 2)
    {
        [TSMessage showNotificationInViewController:self title:[response objectForKey:KMsg] subtitle:@"" type:TSMessageNotificationTypeWarning];
    }
    else if ([[response objectForKey:KStatus] integerValue] == 0)
    {
    }
    }
    @catch(NSException * e)
    {
        NSLog(@"%@", e.debugDescription);
    }
    
    //    [Loader hideLoader];
    //    if ([[response objectForKey:kParameterMessage] length] > 0)
    //    {
    //
    //        //        [Loader showSuccessMessage:[response objectForKey:kParameterMessage]];
    //        [TSMessage showNotificationInViewController:self title:[response objectForKey:kParameterMessage]  subtitle:@"" type:TSMessageNotificationTypeSuccess duration:2.0];
    //    }
    //
    //
    //    if ([[response objectForKey:kParameterStatus] integerValue] == kStatusSuccess)
    //    {
    //
    //        if ([[userInfo objectForKey:KActionkey] isEqualToString:KSocialConfigAction])
    //        {
    //            // need to handle
    //            [[NSUserDefaults standardUserDefaults] setObject:userId forKey:kUserId];
    //            [self setRootNavigationController];
    //        }
    //        else if ([[userInfo objectForKey:KActionkey] isEqualToString:KGetBucketUrlAction])
    //        {
    //            [network setBucketURL:[[response objectForKey:kParameterUserListResult] objectForKey:KBucketURL]];
    //            [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:kParameterUserListResult] objectForKey:KBucketName] forKey:KBucketName];
    //        }
    //        else if ([[userInfo objectForKey:KActionkey] isEqualToString:kActionRegistration])
    //        {
    //
    //            if ([response objectForKey:kParameterUserListResult] != nil)
    //            {
    //
    //                if ([[[response objectForKey:kParameterUserListResult] objectForKey:kParameterMessage] length] > 0)
    //                {
    //
    //                    //                    [Loader showSuccessMessage:[[response objectForKey:kParameterUserListResult] objectForKey:kParameterMessage] ];
    //                    [TSMessage showNotificationInViewController:self title:@"Successful" subtitle:[[response objectForKey:kParameterUserListResult] objectForKey:kParameterMessage] type:TSMessageNotificationTypeSuccess duration:2.0];
    //                }
    //                else if ([[response objectForKey:kParameterMessage] length] > 0)
    //                {
    //                    //                    [Loader showSuccessMessage:[response objectForKey:kParameterMessage]];
    //                    [TSMessage showNotificationInViewController:self title:@"Successful" subtitle:[response objectForKey:kParameterMessage] type:TSMessageNotificationTypeSuccess duration:2.0];
    //
    //                }
    //            }
    //
    //
    //            if ([response objectForKey:kParameterUserListResult] != nil)
    //            {
    //
    //                NSDictionary * friendListDict = [response objectForKey:kParameterUserListResult];
    //                [UserDefault setUserId:[friendListDict objectForKey:kUserId]]; // set user_id in userDefaults
    //
    //            }
    //
    //            //Remove the current navigation controller. - To navigate to home screen after successful
    //
    //            //        MenuViewController * menuVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"MenuViewController"];
    //            //
    //            //        REFrostedViewController * drawerViewController = [CreateDrawer getDrawerInstance:[[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"HomeNavigationControler"] MenuViewController:menuVC];
    //            //        drawerViewController.delegate = self;
    //            //
    //            //
    //            //        //Remove the current navigation controller. - rahul's code
    //            //
    //            //        [self.navigationController.view removeFromSuperview];
    //            //
    //            //        [AppDelegate getWindowInstance].rootViewController = drawerViewController;
    //
    //            [self setRootNavigationController];
    //
    //        }
    //    }
    //    else if ([[response objectForKey:kParameterStatus] integerValue] == 2)
    //    {
    //        //        NSDictionary * friendListDict = [response objectForKey:kParameterUserListResult];
    //        userId = [response objectForKey:@"_user_id"];
    //
    //
    //        UIAlertView * alreadySocialRegistered = [[UIAlertView alloc]initWithTitle:@"Already Registered!" message:@"This Email Address is already regsitered. Do you want to configure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    //        [alreadySocialRegistered show];
    //    }
    //
}


- (void)didFailWithError:(NSError *)error withUserInfo:(NSDictionary *)userInfo
{
    
    //    [Loader hideLoader];
    //    if (error.code == 3840)
    //    {
    //        //        [Loader showFailedMessage:@"Sorry! Something went wrong!"];// server issue - invalid json syntax
    //        [TSMessage showNotificationInViewController:self title:@"Error" subtitle:@"Password length must be atleast 8 or greater characters" type:TSMessageNotificationTypeError duration:2.0];
    //
    //
    //    }
    //    else if (error.code == -1004)
    //    {
    //        //        [Loader showFailedMessage:@"Sorry! Something went wrong!"];// Internet issue - Internet not connected
    //        UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"No Internet Connection" message:@"Please check your internet connection." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Settings", nil];
    //        alertView.tag = 1002;
    //        [alertView show];
    //    }
    //    else
    //    {
    //        //        [Loader showFailedMessage:error.localizedDescription];
    //        [TSMessage showNotificationInViewController:self title:@"Error" subtitle:error.localizedDescription type:TSMessageNotificationTypeError duration:2.0];
    //
    //    }
    
    
}



-(void)setRootNavigationController
{
        MMDrawerController * drawerController;
    
        MenuVC * menuVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"MenuVCID"];
    
    
        UIViewController * centerViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"HomeVCID"];
    
        UIViewController * rightSideDrawerViewController = menuVC;
    
        //    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:centerViewController];
        //    [navigationController setRestorationIdentifier:@"MMExampleCenterNavigationControllerRestorationKey"];
    
        UINavigationController * navigationController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"HomeNavigationControllerID"];
        [AppDelegate appDelegate].homeNavController = navigationController;
        [navigationController setRestorationIdentifier:@"MMExampleCenterNavigationControllerRestorationKey"];
    
        if(SYSTEM_VERSION_GREATER_THAN(@"6.0"))
        {
            UINavigationController * rightSideNavController = [[UINavigationController alloc] initWithRootViewController:rightSideDrawerViewController];
            [rightSideNavController setRestorationIdentifier:@"MMExampleRightNavigationControllerRestorationKey"];
            //        UINavigationController * leftSideNavController = [[MMNavigationController alloc] initWithRootViewController:leftSideDrawerViewController];
            //        [leftSideNavController setRestorationIdentifier:@"MMExampleLeftNavigationControllerRestorationKey"];
            drawerController = [[MMDrawerController alloc]
                                initWithCenterViewController:navigationController
                                leftDrawerViewController:nil
                                rightDrawerViewController:rightSideNavController];
            [drawerController setShowsShadow:NO];
        }
        else{
            drawerController = [[MMDrawerController alloc]
                                initWithCenterViewController:navigationController
                                leftDrawerViewController:nil
                                rightDrawerViewController:rightSideDrawerViewController];
        }
        [drawerController setRestorationIdentifier:@"MMDrawer"];
        [drawerController setMaximumRightDrawerWidth:centerViewController.view.bounds.size.width - 100];
        [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
        [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
        [drawerController
         setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
             MMDrawerControllerDrawerVisualStateBlock block;
             block = [[MMExampleDrawerVisualStateManager sharedManager]
                      drawerVisualStateBlockForDrawerSide:drawerSide];
             if(block){
                 block(drawerController, drawerSide, percentVisible);
             }
         }];
    
        UIWindow * window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        if(SYSTEM_VERSION_GREATER_THAN(@"6.0"))
        {
            //        UIColor * tintColor = [UIColor colorWithRed:59.0/255.0 green:25.0/255.0 blue:47.0/255.0 alpha:1.0];
            UIColor * tintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"header.jpg"]];
            [window setTintColor:tintColor];
        }
    
        [AppDelegate appDelegate].drawerController = drawerController;
    
        //Remove the current navigation controller. - rahul's code
        [self.navigationController.view removeFromSuperview];
        [AppDelegate getWindowInstance].rootViewController = drawerController;
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //    if (buttonIndex == 1)
    //    {
    //        if (alertView.tag == 1002)
    //        {
    //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    //        }
    //        else
    //        {
    //            //        if ([KLoginType isEqualToString:KSocialRegister])
    //            //        {
    //            NSString * socialType;
    //
    //            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"socialType"] isEqualToString:@"facebook"])
    //            {
    //                socialType = KFBId;
    //            }
    //            else if ([[[NSUserDefaults standardUserDefaults] objectForKey:KSocialType] isEqualToString:KGooglePlus])
    //            {
    //                socialType = kGplusId;
    //            }
    //            else if ([[[NSUserDefaults standardUserDefaults] objectForKey:KSocialType] isEqualToString:KTwitter])
    //            {
    //                socialType = KTwitterId;
    //            }
    //            else if ([[[NSUserDefaults standardUserDefaults] objectForKey:KSocialType] isEqualToString:KInstagram])
    //            {
    //                socialType = KSocialconfigInstagram;
    //            }
    //            else if ([[[NSUserDefaults standardUserDefaults] objectForKey:KSocialType] isEqualToString:KDropbox])
    //            {
    //                socialType = KSocialconfigDropbox;
    //            }
    //            else if ([[[NSUserDefaults standardUserDefaults] objectForKey:KSocialType] isEqualToString:KFlickr])
    //            {
    //                socialType = KFlickrId;
    //            }
    //            NSDictionary * configSocialDict = [[NSDictionary alloc] initWithObjectsAndKeys:KSocialConfigAction,kAction,[_userDetailDict objectForKey:@"id"],socialType,userId,kUserId, nil];
    //            [network configSocailRegistration:configSocialDict UserInfo:[[NSDictionary alloc] initWithObjectsAndKeys:KSocialConfigAction,KActionkey,userId,kUserId, nil]];
    //            //        }
    //        }
    //
    //    }
    //    else
    //    {
    //        //        [Loader showFailedMessage:@"Try again resgistering using other EmailId and Username."];
    //        [TSMessage showNotificationInViewController:self title:@"Error" subtitle:@"Try again resgistering using other EmailId and Username." type:TSMessageNotificationTypeError duration:2.0];
    //
    //
    //    }
}

#pragma mark - UIPickerview Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;//Or return whatever as you intend
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (_pickerViewIMType.tag == KPickerViewImA)
    {
        return [iMTypeArr count];
    }
    else if (_pickerViewIMType.tag == KPickerViewGender)
    {
        return [genderArr count];
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (_pickerViewIMType.tag == KPickerViewImA)
    {
        return [NSString stringWithFormat:@"%@",[[iMTypeArr objectAtIndex:row] objectForKey:KUserTypeName]];//Or, your suitable title; like Choice-a, etc.
    }
    else if (_pickerViewIMType.tag == KPickerViewGender)
    {
        return [NSString stringWithFormat:@"%@",[genderArr objectAtIndex:row]];//Or, your suitable title; like Choice-a, etc.
    }
    else
    {
        return [NSString stringWithFormat:@"%@",[genderArr objectAtIndex:row]];//Or, your suitable title; like Choice-a, etc.
    }
    
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    //Here, like the table view you can get the each section of each row if you've multiple sections
    if (_pickerViewIMType.tag == KPickerViewImA)
    {
        NSLog(@"Selected Color: %@. Index of selected color: %ld", [iMTypeArr objectAtIndex:row], (long)row);
        pickerValue = [[iMTypeArr objectAtIndex:row] objectForKey:KUserTypeName];
    }
    else if (_pickerViewIMType.tag == KPickerViewGender)
    {
        NSLog(@"Selected Color: %@. Index of selected color: %ld", [genderArr objectAtIndex:row], (long)row);
        pickerValue = [genderArr objectAtIndex:row];
    }
    else
    {
        NSLog(@"Selected Color: %@. Index of selected color: %ld", [genderArr objectAtIndex:row], (long)row);
        pickerValue = [[genderArr objectAtIndex:row] objectForKey:KUserTypeName];
    }
    
}

#pragma mark
#pragma mark - UITextField delegate methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField       // return NO to disallow editing.
{
    if (textField.tag == KTextFieldImA)
    {
        _pickerViewIMType.tag = KPickerViewImA;
        _viewBasePickerView.hidden = NO;
        [_pickerViewIMType reloadAllComponents];
        return NO;
    }
    else if (textField.tag == KTextFieldGender)
    {
        _pickerViewIMType.tag = KPickerViewGender;
        _viewBasePickerView.hidden = NO;
        [_pickerViewIMType reloadAllComponents];
        return NO;
    }
    else
    {
        return YES;
    }
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField  // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
{
    [textField resignFirstResponder];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField             // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
{
    
    [textField resignFirstResponder];
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string   // return NO to not change text
{
    //    if (textField.tag != 2)
    //    {
    //        textField.text = [textField.text capitalizedString];
    //    }
    return YES;
    
}


- (BOOL)textFieldShouldClear:(UITextField *)textField               // called when clear button pressed. return NO to ignore (no notifications)
{
    
    [textField resignFirstResponder];
    return YES;
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    //    if (textField.tag != 2)
    //    {
    //        LoginCell *cell = (LoginCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    //        
    //        UIView * tf = [cell viewWithTag:textField.tag + 1];
    //        [tf becomeFirstResponder];
    //    }
    //    else if (textField.tag == 2)
    //    {
    //        if (textField.returnKeyType == UIReturnKeyGo)
    //        {
    //            [self loginButtonClicked:nil];
    //        }
    //    }
    return YES;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
