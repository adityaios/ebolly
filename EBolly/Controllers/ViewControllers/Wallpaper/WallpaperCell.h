//
//  WallpaperCell.h
//  EBolly
//
//  Created by Yashvir on 13/08/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WallpaperCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end
