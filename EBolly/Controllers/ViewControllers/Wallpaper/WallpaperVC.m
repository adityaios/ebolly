//
//  WallpaperVC.m
//  EBolly
//
//  Created by Yashvir on 10/08/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import "WallpaperVC.h"
#import "WallpaperCell.h"
#import "UserDefault.h"
#import "AppDelegate.h"
#import "MMDrawerBarButtonItem.h"
#import "UIViewController+MMDrawerController.h"
#import "Network.h"
#import "TSMessage.h"
#import "GetWallpaperReq.h"
#import "UIImageView+WebCache.h"

#import "MWPhotoBrowser.h"
#import "MWCommon.h"
#import <AssetsLibrary/AssetsLibrary.h>

static const NSInteger KPostPerPageCount = 20;

@interface WallpaperVC ()<NetworkDelegate, MWPhotoBrowserDelegate>
{
    Network * network;
    UIRefreshControl * refreshControl;
    
    BOOL isReload;
    BOOL isfetchingNewData;
    
    NSMutableArray * dataArr;
    
    UISegmentedControl *_segmentedControl; // photo browser
    NSMutableArray *_selections; // photo browser
    MWPhotoBrowser * browser; // photo browser
    
    NSInteger selectedIndex;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) NSMutableArray *photos; // photo browser
@property (nonatomic, strong) NSMutableArray *thumbs; // photo browser
@property (nonatomic, strong) ALAssetsLibrary *assetLibrary; // photo browser
@property (nonatomic) int photosID; // photo browser
@property (nonatomic) NSInteger photoIndex; // photo browser

@end

@implementation WallpaperVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self initializeVariables];
    [self viewSetUp];
    [self restrictRotation:YES];
}

#pragma mark - Layout Orientation
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void) restrictRotation:(BOOL) restriction
{
    AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.restrictRotation = restriction;
}


#pragma mark - SetUpView

- (void) initializeVariables
{
    AppDelegate* sharedDelegate = [AppDelegate appDelegate];
    sharedDelegate.homeNavController = self.navigationController;
    
    network = [Network new];
    network.delegate = self;
    
    dataArr  = [NSMutableArray new];
    
    selectedIndex = 0;
    
    refreshControl = [[UIRefreshControl alloc]init];
    refreshControl.tintColor = [UIColor colorWithRed:70.0/255.0 green:17.0/255.0 blue:38.0/255.0 alpha:1.0];
    UIColor *fg = [UIColor colorWithRed:70.0/255.0 green:17.0/255.0 blue:38.0/255.0 alpha:1.0];
    NSDictionary *attrsDictionary = @{NSForegroundColorAttributeName: fg};
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull To Refresh...." attributes:attrsDictionary];
    [_collectionView addSubview:refreshControl];
    _collectionView.alwaysBounceVertical = YES;
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    isReload = NO; // refresh Flag
    isfetchingNewData = NO;

    [self addBackBarBtn];
    
    [self calGetWallpaperWs];
}

-(void) addBackBarBtn
{
    UIButton * backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 50, 20)];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundColor:[UIColor clearColor]];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"OpenSans-CondensedBold" size:15.0]];
    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
}

-(void)back
{
    [self restrictRotation:YES];// set orientation
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)refreshTable
{
    //TODO: refresh your data
    @try
    {
        if(!isfetchingNewData)
        {
            isReload = YES;
            
            GetWallpaperReq * req = [GetWallpaperReq new];
            req.posts_per_page = [NSString stringWithFormat:@"%ld",(long)KPostPerPageCount];
            req.paged = @"1";
            
            [network getWallpapers:req UserInfo:[[NSDictionary alloc] initWithObjectsAndKeys:KGetPostsAction,KAction, nil]];
        }
        else
        {
            [refreshControl endRefreshing];
        }
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.debugDescription);
    }
    
    
}


-(void) calGetWallpaperWs
{
    GetWallpaperReq * req = [GetWallpaperReq new];
    req.posts_per_page = [NSString stringWithFormat:@"%ld",(long)KPostPerPageCount];
    
    NSInteger pageIndex = 0;
    if( [dataArr count] % KPostPerPageCount != 0)
    {

        if ([dataArr count] < KPostPerPageCount)
        {
            req.paged = @"1";
        }
        else
        {
            pageIndex = [dataArr count] / KPostPerPageCount;
            pageIndex++;
            req.paged = [NSString stringWithFormat:@"%ld",(long)pageIndex];
        }
    }
    else
    {
        pageIndex = [dataArr count] / KPostPerPageCount;
        pageIndex++;
        req.paged = [NSString stringWithFormat:@"%ld",(long)pageIndex];
    }
    
    [network getWallpapers:req UserInfo:[[NSDictionary alloc] initWithObjectsAndKeys:KGetPostsAction,KAction,[NSString stringWithFormat:@"%ld",(long)pageIndex],KPageIndex, nil]];
}


- (void) viewSetUp
{
    UITapGestureRecognizer * twoFingerDoubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(twoFingerDoubleTap:)];
    [twoFingerDoubleTap setNumberOfTapsRequired:2];
    [twoFingerDoubleTap setNumberOfTouchesRequired:2];
    [self.view addGestureRecognizer:twoFingerDoubleTap];
    
    [self setupRightMenuButton];
    
    self.title = NSLocalizedString(@"Wallpapers", @"Wallpapers");
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]}; //set color of navigation bar title color.
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageNamed:@"navBarStrip.jpg"]
                                                                 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch] forBarMetrics:UIBarMetricsDefault];
}


- (void)setupLeftMenuButton
{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}


- (void)setupRightMenuButton
{
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    [rightDrawerButton setTintColor:[UIColor whiteColor]];
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}


#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

-(void)doubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideLeft completion:nil];
}

-(void)twoFingerDoubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideRight completion:nil];
}


- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark - CollectionView datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [dataArr count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    WallpaperCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:[[[[dataArr objectAtIndex:indexPath.row] objectForKey:KImagesArray] objectForKey:KThumbnail] objectForKey:KUrl]] placeholderImage:[UIImage imageNamed:@"logo.png"]];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndex = indexPath.item;
    [self showPhotoBrowser:dataArr];
}

#pragma mark - Network Delegate
-(void) didReceiveResponse:(NSDictionary *)response withUserInfo:(NSDictionary *)userInfo
{
// success
    if (refreshControl != nil)
    {
        [refreshControl endRefreshing];
    }
    
    if ([[[[response objectForKey:KStatus] stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString] isEqualToString:@"OK"])
    {
        
        if ([[userInfo objectForKey:KAction] isEqualToString:KGetPostsAction])
        {
            dataArr =  [NSMutableArray arrayWithArray:dataArr];
            
            if(isReload)
            {
                [dataArr removeAllObjects];
                if ([NSArray arrayWithArray:[response objectForKey:KResultWallpapers]] != nil)
                {
                    [dataArr  addObjectsFromArray:[response objectForKey:KResultWallpapers]];
                }
            }
            else
            {
                NSInteger pageIndex = 0;
                if( [dataArr count] % KPostPerPageCount != 0)
                {
                    
                    if ([dataArr count] < KPostPerPageCount)
                    {
                        [dataArr removeAllObjects];
                        [dataArr  addObjectsFromArray:[response objectForKey:KResultWallpapers]];
                    }
                    else
                    {
                        pageIndex = [dataArr count] / KPostPerPageCount;
                        NSMutableArray * newArray = [NSMutableArray arrayWithArray:[dataArr subarrayWithRange:NSMakeRange(0, (pageIndex * KPostPerPageCount))]];
                        
                        dataArr = newArray;
                        [dataArr  addObjectsFromArray:[response objectForKey:KResultWallpapers]];
                    }
                }
                else
                {
                        [dataArr  addObjectsFromArray:[response objectForKey:KResultWallpapers]];
                }

            }
            
            [_collectionView reloadData];
        }
    }
    else
    {
        [TSMessage showNotificationInViewController:self title:[response objectForKey:KMsg] subtitle:@"" type:TSMessageNotificationTypeWarning];
    }

    if(isReload)
    {
        isReload = NO;
    }
    if(isfetchingNewData)
    {
        isfetchingNewData = NO;
    }

}

-(void)didFailWithError:(NSError *)error withUserInfo:(NSDictionary *)userInfo
{
// failure
    
    if (refreshControl != nil)
    {
        [refreshControl endRefreshing];
    }
    
    if(isReload)
    {
        isReload = NO;
    }
    if(isfetchingNewData)
    {
        isfetchingNewData = NO;
    }


    if (error.code == 3840)
    {
        [TSMessage showNotificationInViewController:self title:@"Error" subtitle:@"Sorry! Something went wrong!" type:TSMessageNotificationTypeError duration:2.0];
    }
    else if (error.code == -1004)
    {
        UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"No Internet Connection" message:@"Please check your internet connection." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Settings", nil];
        alertView.tag = 1002;
        [alertView show];
    }
    else
    {
        [TSMessage showNotificationInViewController:self title:@"Error" subtitle:error.localizedDescription type:TSMessageNotificationTypeError duration:2.0];
        
    }

}


#pragma mark - Photo Browser
-(void)showPhotoBrowser:(NSArray *)photoArray
{
    // Browser
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    NSMutableArray *thumbs = [[NSMutableArray alloc] init];
    MWPhoto *photo;
    BOOL displayActionButton = NO;
    BOOL displaySelectionButtons = NO;
    BOOL displayNavArrows = NO;
    BOOL enableGrid = NO;
    BOOL startOnGrid = NO;
    // Photos
    
    for (int index = 0 ; index < [photoArray count] ; index++)
    {
        NSURL * url = [NSURL URLWithString:[[[[photoArray objectAtIndex:index] objectForKey:KImagesArray] objectForKey:KPostThumbnail] objectForKey:KUrl ]];
        photo = [MWPhoto photoWithURL:url];
        [photos addObject:photo];
        [thumbs addObject:[MWPhoto photoWithURL:url]];
    }
    
    self.photos = photos;
    self.thumbs = thumbs;
    
    // Create browser
    browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = displayActionButton;
    browser.displayNavArrows = displayNavArrows;
    browser.displaySelectionButtons = displaySelectionButtons;
    browser.alwaysShowControls = displaySelectionButtons;
    browser.zoomPhotosToFill = YES;
    browser.photosArray = [[NSMutableArray alloc] initWithArray:dataArr]; // rahul added to show photo detail page after photo browser
    
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    browser.wantsFullScreenLayout = YES;
#endif
    browser.enableGrid = enableGrid;
    browser.startOnGrid = startOnGrid;
    browser.enableSwipeToDismiss = YES;
    [browser setCurrentPhotoIndex:selectedIndex];
    // Reset selections
    if (displaySelectionButtons) {
        _selections = [NSMutableArray new];
        for (int i = 0; i < photos.count; i++) {
            [_selections addObject:[NSNumber numberWithBool:NO]];
        }
    }
    [self.navigationController pushViewController:browser animated:YES];
    
}


#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < _thumbs.count)
        return [_thumbs objectAtIndex:index];
    return nil;
}

- (MWCaptionView *)photoBrowser:(MWPhotoBrowser *)photoBrowser captionViewForPhotoAtIndex:(NSUInteger)index {
    MWPhoto *photo = [self.photos objectAtIndex:index];
    MWCaptionView *captionView = [[MWCaptionView alloc] initWithPhoto:photo];
    return captionView;
}

-(void)hello
{
    
}

//- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser actionButtonPressedForPhotoAtIndex:(NSUInteger)index {
//    NSLog(@"ACTION!");
//}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
}

- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index {
    return [[_selections objectAtIndex:index] boolValue];
}

//- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser titleForPhotoAtIndex:(NSUInteger)index {
//    return [NSString stringWithFormat:@"Photo %lu", (unsigned long)index+1];
//}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index selectedChanged:(BOOL)selected {
    [_selections replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:selected]];
    NSLog(@"Photo at index %lu selected %@", (unsigned long)index, selected ? @"YES" : @"NO");
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
    NSLog(@"Did finish modal presentation");
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Scroll view delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // UITableView only moves in one direction, y axis
    // Change 10.0 to adjust the distance from bottom
    @try
    {
        if(!isReload)
        {
            if (isfetchingNewData == NO)
            {
                if ((scrollView.contentSize.height - scrollView.frame.size.height) - (scrollView.contentOffset.y) <= 10.0)
                {
                    isfetchingNewData = YES;
                    
                    [self calGetWallpaperWs];
                }
            }
            else
            {
                NSLog(@"Api already fired - IGNORING DUPLICATE HIT.");
            }
        }
        else
        {
               NSLog(@"Reload Api already fired - IGNORING DUPLICATE HIT.");
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.debugDescription);
    }
    
}




#pragma mark - Did Receive Memory Warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
