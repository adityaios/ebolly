//
//  BasicUtilities.h
//  focvs
//
//  Created by NM8 on 10/01/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CTAssetsPickerController.h"
#import "CTAssetsPageViewController.h"

@interface BasicUtilities : NSObject

+(void)showAlertView: (NSString *)title message: (NSString *)message cancel: (NSString *)cancelString;
+(void)showConfirmationAlertView: (NSString *)title message: (NSString *)message cancel: (NSString *)cancelString Ok:(NSString *)okString;
+ (BOOL)validateEmailWithString:(NSString*)email;
+(BOOL) validateMobileNumber:(NSString *) mobileNumberStr;
+ (void) setImageOverImageView:(UIImageView *)imageView avatarImage:(UIImage *)avatarImage urlString: (NSString *) url;
+ (void) runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
+ (void) putDataFromAssertUrl:(NSURL*)url ToImageView:(UIImageView*) imageView;
+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width;
+ (UIImage*) scaleImage:(UIImage*)image toSize:(CGSize)newSize;
+(UIButton *) addCustomBtn: (CGRect)frame image: (UIImage *)buttonImage btnTitle: (NSString *)btnTitle;
+(UILabel *) addCustomLabel: (CGRect)frame titleText: (NSString *)titleText;
+ (void) addNoObjectFoundCustomView: (CGRect)frame bgColor: (UIColor *)bgColor customButton: (UIButton *)customButton customLabel:(UILabel *)customLabel;

// social Sharing Methods
+ (void)uploadToFacebook: (UIImage *)imageToBeShared;
+ (BOOL)saveImageToDocumentDirectory: (UIImage*)image imageName:(NSString *)imageName;
+ (BOOL)saveImageToDocumentDirectoryWithoutExistanceCheck: (UIImage*)image imageName:(NSString *)imageName;
+ (UIImage*)loadImageFromDocumentDirectory :(NSString *)imageName;
+ (void)removeImageFromDocumentDirectory:(NSString *)fileName;
+(void)deleteAllFilesOfDirectory;
+(BOOL)createSubDirectory:(NSString *)folderName;
+(BOOL) deleteSubDirectory:(NSString *)folderName;
+ (NSString *) getDirectoryPath;
+ (unsigned long long int)folderSizeWithAvoidingSizeOfSqliteFiles:(NSString *)folderPath;
+ (unsigned long long int) getMBFromBytes:(unsigned long long int)bytes;
+(NSUInteger)calculatedImageMemorySize:(UIImage *)image;
+ (UIImage *) scalingAndCroppingToSize:(CGSize)size image:(UIImage * )image;
+(NSString *)getFileName; //get unique File name
@end
