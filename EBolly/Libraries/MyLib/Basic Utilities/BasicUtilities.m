//
//  BasicUtilities.m
//  focvs
//
//  Created by NM8 on 10/01/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import "BasicUtilities.h"
#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

// social Sharing imports
#import <FacebookSDK/FacebookSDK.h>

@implementation BasicUtilities

+(void)showAlertView: (NSString *)title message: (NSString *)message cancel: (NSString *)cancelString
{
    UIAlertView * customAlertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:cancelString otherButtonTitles: nil];
    [customAlertView show];
}

+(void)showConfirmationAlertView: (NSString *)title message: (NSString *)message cancel: (NSString *)cancelString Ok:(NSString *)okString
{
    UIAlertView * customAlertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:cancelString otherButtonTitles:@"Ok", nil];
    [customAlertView show];
}


+ (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+(BOOL) validateMobileNumber:(NSString *) mobileNumberStr
{
    NSString *phoneRegex = @"[235689][0-9]{6}([0-9]{3})?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return  [test evaluateWithObject:mobileNumberStr];
}

+ (void) setImageOverImageView: (UIImageView *)imageView avatarImage:(UIImage *)avatarImage urlString: (NSString *) url
{
    // async thumbnail image downloading - sdwebImage lib. - BLOCK START
    if ([NSURL URLWithString:url])
    {
//        __block UIActivityIndicatorView *activityIndicator;
//        __weak UIImageView *weakImageView = imageView;
//        [imageView sd_setImageWithURL:[NSURL URLWithString:url]
//                        placeholderImage:avatarImage
//                                 options:SDWebImageProgressiveDownload
//                                progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//                                    if (!activityIndicator) {
//                                        [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
//                                        activityIndicator.center = weakImageView.center;
//                                        [activityIndicator startAnimating];
//                                    }
//                                }
//                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                                   [activityIndicator removeFromSuperview];
//                                   activityIndicator = nil;
//                               }];
        
        [imageView sd_setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatarGray.png"]];
        
    } // BLOCK END
    else {
        imageView.image = avatarImage;
    }

}


#pragma mark - Hud animation Logic
+ (void) runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}


+(void) putDataFromAssertUrl:(NSURL*)url ToImageView:(UIImageView*) imageView // url is an assert url, can't be directly used.
{
    ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
    [assetLibrary assetForURL:url resultBlock:^(ALAsset *asset)
     {
         ALAssetRepresentation *rep = [asset defaultRepresentation];
         Byte *buffer = (Byte*)malloc(rep.size);
         NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
         NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];//this is NSData may be what you want
         imageView.image = [UIImage imageWithData:data];
     }
                 failureBlock:^(NSError *err)
     {
         NSLog(@"Error: %@",[err localizedDescription]);
     }];
}

+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage*) scaleImage:(UIImage*)image toSize:(CGSize)newSize {
    CGSize scaledSize = newSize;
    float scaleFactor = 1.0;
    if( image.size.width > image.size.height ) {
        scaleFactor = image.size.width / image.size.height;
        scaledSize.width = newSize.width;
        scaledSize.height = newSize.height / scaleFactor;
    }
    else {
        scaleFactor = image.size.height / image.size.width;
        scaledSize.height = newSize.height;
        scaledSize.width = newSize.width / scaleFactor;
    }
    
    UIGraphicsBeginImageContextWithOptions( scaledSize, NO, 0.0 );
    CGRect scaledImageRect = CGRectMake( 0.0, 0.0, scaledSize.width, scaledSize.height );
    [image drawInRect:scaledImageRect];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

+(UIButton *) addCustomBtn: (CGRect)frame image: (UIImage *)buttonImage btnTitle: (NSString *)btnTitle
{
    UIButton * plusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    plusBtn = [[UIButton alloc]initWithFrame:frame];
    [plusBtn setTitle:btnTitle forState:UIControlStateNormal];
//    [plusBtn setImage:buttonImage forState:UIControlStateNormal];
    [plusBtn setContentMode:UIViewContentModeScaleAspectFit];
    [plusBtn setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [plusBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    return plusBtn;
}

+(UILabel *) addCustomLabel: (CGRect)frame titleText: (NSString *)titleText
{
    UILabel * label = [[UILabel alloc]initWithFrame:frame];
    label.text = titleText;
    label.font = [UIFont fontWithName:@"OpenSans-CondLight" size:18.0];
    [label setTextColor:[UIColor colorWithRed:70.0/255.0 green:17.0/255.0 blue:38.0/255.0 alpha:1.0]];
    label.textColor = [UIColor colorWithRed:70.0/255.0 green:17.0/255.0 blue:38.0/255.0 alpha:1.0];
    return label;
}


+ (void) addNoObjectFoundCustomView: (CGRect)frame bgColor: (UIColor *)bgColor customButton: (UIButton *)customButton customLabel:(UILabel *)customLabel
{
    UIView * customView = [[UIView alloc]initWithFrame:frame];
    if (bgColor != nil)
    {
        [customView setBackgroundColor:bgColor];
    }
    [customView addSubview:customButton];
    [customView addSubview:customLabel];
}


#pragma mark - Social Sharing 

#pragma mak - Facebook
+ (void)uploadToFacebook: (UIImage *)imageToBeShared
{
    
    
    NSArray *Permissions = @[@"public_profile", @"email"];
    
    [FBSession openActiveSessionWithPublishPermissions:Permissions defaultAudience:FBSessionDefaultAudienceFriends allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
        //        [session closeAndClearTokenInformation];
        NSLog(@"done");
        //        NSLog(@"token : %@",session.accessTokenData.accessToken);
        if(!session.accessTokenData.accessToken){
            //            _fbBlock(nil);
            return ;
        }
        
        switch (status) {
            case FBSessionStateClosed:
                //need to handle
                break;
            case FBSessionStateCreated:
                break;
            case  FBSessionStateCreatedOpening:
                break;
            case FBSessionStateClosedLoginFailed:
                [FBSession.activeSession closeAndClearTokenInformation];
                //need to handle
                break;
            case FBSessionStateOpen:
                [[FBRequest requestForMe] startWithCompletionHandler:
                 ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
                     
                     NSLog(@"user :%@",user); // here got the user Info.
                        if ([FBSession.activeSession.permissions
                              indexOfObject:@"publish_actions"] == NSNotFound) {
                             
                             NSArray *permissions =
                             [NSArray arrayWithObjects:@"publish_actions",@"publish_stream", nil];
                             [[FBSession activeSession] requestNewPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceFriends
                                                                   completionHandler:^(FBSession *session, NSError *error) {}];
                         }
                         else
                         {
                             NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
                             [params setObject:@"Photo post test..." forKey:@"message"];
                             [params setObject:[UIImage imageNamed:@"Avatar2.png"] forKey:@"picture"];
                             //for not allowing multiple hits
                             
                             [FBRequestConnection startWithGraphPath:@"me/photos"
                                                          parameters:params
                                                          HTTPMethod:@"POST"
                                                   completionHandler:^(FBRequestConnection *connection,
                                                                       id result,
                                                                       NSError *error)
                              {
                                  if (error)
                                  {
                                      //showing an alert for failure
                                      UIAlertView *alertView = [[UIAlertView alloc]
                                                                initWithTitle:@"Post Failed"
                                                                message:error.localizedDescription
                                                                delegate:nil
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                                      [alertView show];
                                      
                                  }
                                  else
                                  {
                                      //showing an alert for success
                                      UIAlertView *alertView = [[UIAlertView alloc]
                                                                initWithTitle:@"Post success"
                                                                message:@"Shared the photo successfully"
                                                                delegate:nil
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                                      [alertView show];
                                      
                                  }
                              }];
                         }

                 }];
                break;
        }
        
    }];
   
}

+ (BOOL)saveImageToDocumentDirectory: (UIImage*)image imageName:(NSString *)imageName
{
    if (image != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
        NSData* data = UIImageJPEGRepresentation(image, 0.5);
        
        if (![self loadImageFromDocumentDirectory:imageName])
        {
//            BOOL saved = [data writeToFile:path atomically:YES];
            NSError *error;
            return [data writeToFile:path options:NSDataWritingAtomic error:&error];
        }
        else
        {
            NSLog(@"image already present");
            return YES;
        }
    }
        return NO;
}

+ (BOOL)saveImageToDocumentDirectoryWithoutExistanceCheck: (UIImage*)image imageName:(NSString *)imageName
{
    @autoreleasepool
    {
        
        if (image != nil)
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
            NSData* data = UIImageJPEGRepresentation(image, 0.5);
            NSError *error;
            return [data writeToFile:path options:NSDataWritingAtomic error:&error];
        }
        
    }
    return NO;
}


+ (UIImage*)loadImageFromDocumentDirectory :(NSString *)imageName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

+ (void)removeImageFromDocumentDirectory:(NSString *)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success)
    {
//        UIAlertView *removeSuccessFulAlert=[[UIAlertView alloc]initWithTitle:@"Congratulation:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//        [removeSuccessFulAlert show];
        NSLog(@"Successfully removed file at %@",filePath);
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

+ (void)deleteAllFilesOfDirectory
{
    // get all files in this directory
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSArray *fileArray = [fileManager contentsOfDirectoryAtPath:documentsPath error:nil];
    for (NSString *filename in fileArray)
    {
        [fileManager removeItemAtPath:[documentsPath stringByAppendingPathComponent:filename] error:NULL];
    }
}

+ (BOOL)createSubDirectory:(NSString *)folderName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:folderName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        NSError * error;
        return [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    return nil;
}

+(BOOL) deleteSubDirectory:(NSString *)folderName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:folderName];

    return [[NSFileManager defaultManager] removeItemAtPath:dataPath error:nil];
}

+ (NSString *) getDirectoryPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
   return [paths objectAtIndex:0];
}

+ (unsigned long long int)folderSizeWithAvoidingSizeOfSqliteFiles:(NSString *)folderPath
{
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject])
    {
        if (!([[fileName pathExtension] isEqualToString:@"sqlite"] || [[fileName pathExtension] isEqualToString:@"sqlite-shm"] || [[fileName pathExtension] isEqualToString:@"sqlite-wal"] || [[fileName pathExtension] isEqualToString:@"txt"])) // get size of only cached images
        {
            NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:nil];
            fileSize += [fileDictionary fileSize];
        }
    }
    
    return fileSize;
}

+ (unsigned long long int) getMBFromBytes:(unsigned long long int)bytes;
{
    return bytes/(1024*1024);
}

+(NSUInteger)calculatedImageMemorySize:(UIImage *)image
{
    return CGImageGetHeight(image.CGImage) * CGImageGetBytesPerRow(image.CGImage);
}

#pragma mark - scale and crop image

+ (UIImage *) scalingAndCroppingToSize:(CGSize)size image:(UIImage * )image
{
    if (CGSizeEqualToSize(image.size, size) || CGSizeEqualToSize(size, CGSizeZero)) {
        return image;
    }
    
    CGSize scaledSize = size;
    CGPoint thumbnailPoint = CGPointZero;
    
    CGFloat widthFactor = size.width / image.size.width;
    CGFloat heightFactor = size.height / image.size.height;
    CGFloat scaleFactor = (widthFactor > heightFactor) ? widthFactor : heightFactor;
    scaledSize.width = image.size.width * scaleFactor;
    scaledSize.height = image.size.height * scaleFactor;
    
    if (widthFactor > heightFactor) {
        thumbnailPoint.y = (size.height - scaledSize.height) * 0.5;
    }
    else if (widthFactor < heightFactor) {
        thumbnailPoint.x = (size.width - scaledSize.width) * 0.5;
    }
    
//    NSMutableArray *scaledImages = [NSMutableArray array];
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    
//    for (UIImage *image in self.images) {
        [image drawInRect:CGRectMake(thumbnailPoint.x, thumbnailPoint.y, scaledSize.width, scaledSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        
//        [scaledImages addObject:newImage];
//    }
    
    UIGraphicsEndImageContext();
    
//    return [UIImage animatedImageWithImages:scaledImages duration:self.duration];
    return newImage;
}

+(NSString *)getFileName
{
    return [[[NSProcessInfo processInfo] globallyUniqueString] stringByAppendingString:@".jpeg"];
}


@end
