//
//  CoreDataHelper.h
//  CoreDataDemo
//
//  Created by Team on 14/04/15.
//  Copyright (c) 2015 Team. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataHelper : NSObject

+ (id) sharedAppDelegate;
+ (void) updateDataInEntity: (NSString *)entityName oldAttribute:(NSString *)oldAttribute oldAttributeValue:(NSString *)oldAttributeValue attributesArray:(NSArray *)attributesArray attributesValueArray:(NSArray *)attributesValueArray;
+ (void)saveDataInEntity:(NSString *)entityName attributesArray:(NSArray *)attributes attributesValue:(NSArray *)attributesValue;
+ (NSArray *)findContact:(NSString *)attributeName value: (NSString *) attributeValue EntityName: (NSString *)entityName;
+ (NSArray *) getObjectsList: (NSString *) entityDescription;
+ (NSUInteger) getEntityObjectsCount: (NSString *) entityDescription;
+ (void) deleteObject: (NSString *) entityDescription index:(NSInteger)index;
+ (void)deleteManagedObject:(NSManagedObject *)managedObject;
+ (void) deleteAllObjects: (NSString *) entityDescription;
+ (void)deleteAllObjectsInCoreData;
+ (void)deleteAllObjectsInContext:(NSManagedObjectContext *)context usingModel:(NSManagedObjectModel *)model;
+ (void)deleteAllObjectsWithEntityName:(NSString *)entityName inContext:(NSManagedObjectContext *)context;
+ (NSManagedObjectModel *)_model;
+ (void) deleteAllFavImagesAndTheirRecordsFromCoreDataOnLogout;
+ (void) deleteCachedImagesAndItsRecordsFromCoreDataAndDD:(NSString *)fileName;
@end
