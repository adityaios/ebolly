//
//  CoreDataHelper.m
//  CoreDataDemo
//
//  Created by Team on 14/04/15.
//  Copyright (c) 2015 Team. All rights reserved.
//

#import "CoreDataHelper.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "BasicUtilities.h"

@implementation CoreDataHelper

static AppDelegate * appDelegate = nil;

+ (id) sharedAppDelegate
{
    if (! appDelegate)
    {
//        appDelegate = [[UIApplication sharedApplication] delegate];
        appDelegate = [AppDelegate appDelegate];
    }
    return appDelegate;
}


#pragma mark - Update Object
+ (void) updateDataInEntity: (NSString *)entityName oldAttribute:(NSString *)oldAttribute oldAttributeValue:(NSString *)oldAttributeValue attributesArray:(NSArray *)attributesArray attributesValueArray:(NSArray *)attributesValueArray
{
    NSArray * objectArr = [self findContact:oldAttribute value:oldAttributeValue  EntityName:entityName];
    
    AppDelegate *appDelegate = [CoreDataHelper sharedAppDelegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    for ( NSManagedObject * managedObject in objectArr)
    {
        for (NSUInteger index = 0; index <  [attributesArray count]; index++)
        {
            [managedObject setValue:[attributesValueArray objectAtIndex:index] forKey:[attributesArray objectAtIndex:index]];
        }
        NSError *error;
        [context save:&error];
        NSLog(@"Updated");
        
    }
}

//#pragma mark - Insert Object
//- (IBAction)saveData:(id)sender
//{
//    AppDelegate *appDelegate =
//    [[UIApplication sharedApplication] delegate];
//
//    NSManagedObjectContext *context = [appDelegate managedObjectContext];
//    NSManagedObject *newContact;
//    newContact = [NSEntityDescription  insertNewObjectForEntityForName:@"Person" inManagedObjectContext:context];
//
//    [newContact setValue: @"Daanish" forKey:@"name"];
//    [newContact setValue: @"hello" forKey:@"address"];
//
//    NSError *error;
//    [context save:&error];
//    NSLog(@"Contact saved");
//}

#pragma mark - Insert Object
+ (void)saveDataInEntity:(NSString *)entityName attributesArray:(NSArray *)attributes attributesValue:(NSArray *)attributesValue
{
    @try
    {
        AppDelegate *appDelegate = [CoreDataHelper sharedAppDelegate];
        NSManagedObjectContext *context = [appDelegate managedObjectContext];
        
        NSManagedObject *newContact;
        newContact = [NSEntityDescription  insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
        
        for (NSUInteger index = 0; index <  [attributes count]; index++)
        {
            [newContact setValue: [attributesValue objectAtIndex:index] forKey: [attributes objectAtIndex:index]];
            
        }
        NSError *error;
        [context save:&error];
        NSLog(@"saved");

    }
    @catch (NSException *exception)
    {
        NSLog(@"%@",exception.debugDescription);
    }
    
}


#pragma mark - Search Object
// Here attributeName is Hardcoded - Need to make it dynamic
+ (NSArray *)findContact:(NSString *)attributeName value: (NSString *) attributeValue EntityName: (NSString *)entityName
{
    AppDelegate *appDelegate = [CoreDataHelper sharedAppDelegate];
    
    NSManagedObjectContext *context =  [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =    [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
//    NSString *string = [NSString stringWithFormat:@"%@", attributeName];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", string];
    
//    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(action = %@)", attributeValue];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(%K = %@)",attributeName, attributeValue];
    
    [request setPredicate:pred];
    NSManagedObject *matches = nil;
    
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request  error:&error];
    
//    if ([objects count] == 0) {
//        NSLog(@"No matches");
//    } else {
//        matches = objects[0];
//        NSLog(@"%@",[matches valueForKey:@"address"]);
//        NSLog(@"%@",[matches valueForKey:@"name"]);
//        NSLog([NSString stringWithFormat:
//               @"%lu matches found", (unsigned long)[objects count]]);
//    }
    return objects;
}


#pragma mark - Get Array of all Objects
+ (NSArray *) getObjectsList: (NSString *) entityDescription
{
    AppDelegate *appDelegate = [CoreDataHelper sharedAppDelegate];
    NSManagedObjectContext *context =  [appDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    return items;
}


#pragma mark - Get Objects List count
+ (NSUInteger) getEntityObjectsCount: (NSString *) entityDescription
{
    AppDelegate *appDelegate = [CoreDataHelper sharedAppDelegate];
    NSManagedObjectContext *context =  [appDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    return [items count];
}

#pragma mark - Delete Methods Core Data
+ (void) deleteObject: (NSString *) entityDescription index:(NSInteger)index
{
    AppDelegate *appDelegate = [CoreDataHelper sharedAppDelegate];
    NSManagedObjectContext *context =  [appDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    
    //    for (NSUInteger index = 0; index < [items count]; index++)
    {
        NSManagedObject *managedObject = [items objectAtIndex:index];
        [context deleteObject:managedObject];
        NSLog(@"%@ object deleted",entityDescription);
    }
    if (![context save:&error]) {
        NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }
    
}


+(void)deleteManagedObject:(NSManagedObject *)managedObject
{
    AppDelegate *appDelegate = [CoreDataHelper sharedAppDelegate];
    NSManagedObjectContext *context =  [appDelegate managedObjectContext];
    [context deleteObject:managedObject];
    
    NSError *error;
    if (![context save:&error])
    {
        // Handle the error.
        NSLog(@"Managed Object deleted from coreData: %@", error.debugDescription);
    }
    
}


+ (void) deleteAllObjects: (NSString *) entityDescription
{
    AppDelegate *appDelegate = [CoreDataHelper sharedAppDelegate];
    NSManagedObjectContext *context =  [appDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject *managedObject in items) {
        [context deleteObject:managedObject];
        NSLog(@"%@ object deleted",entityDescription);
    }
    if (![context save:&error]) {
        NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }
}


// delete all objects of all the the entities
+ (void)deleteAllObjectsInCoreData
{
    AppDelegate *appDelegate = [CoreDataHelper sharedAppDelegate];
    NSManagedObjectContext *context =  [appDelegate managedObjectContext];
    
    NSArray *allEntities = [appDelegate managedObjectModel].entities;
    for (NSEntityDescription *entityDescription in allEntities)
    {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entityDescription];
        
        fetchRequest.includesPropertyValues = NO;
        fetchRequest.includesSubentities = NO;
        
        NSError *error;
        NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
        
        if (error) {
            NSLog(@"Error requesting items from Core Data: %@", [error localizedDescription]);
        }
        
        for (NSManagedObject *managedObject in items) {
            [context deleteObject:managedObject];
        }
        
        if (![context save:&error]) {
            NSLog(@"Error deleting %@ - error:%@", entityDescription, [error localizedDescription]);
        }
    }
}

+ (void)deleteAllObjectsInContext:(NSManagedObjectContext *)context usingModel:(NSManagedObjectModel *)model
{
    NSArray *entities = model.entities;
    for (NSEntityDescription *entityDescription in entities) {
        [self deleteAllObjectsWithEntityName:entityDescription.name
                                   inContext:context];
    }
}

+ (void)deleteAllObjectsWithEntityName:(NSString *)entityName inContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest =
    [NSFetchRequest fetchRequestWithEntityName:entityName];
    fetchRequest.includesPropertyValues = NO;
    fetchRequest.includesSubentities = NO;
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject *managedObject in items) {
        [context deleteObject:managedObject];
        NSLog(@"Deleted %@", entityName);
    }
}

#pragma mark - Create Entity
+ (NSManagedObjectModel *)_model
{
    AppDelegate *appDelegate = [CoreDataHelper sharedAppDelegate];
    NSManagedObjectModel *model = [appDelegate managedObjectModel];
    
    // create the entity
    NSEntityDescription *entity = [[NSEntityDescription alloc] init];
    [entity setName:@"RahulChona"];
    [entity setManagedObjectClassName:@"DTCachedFile"];
    
    // create the attributes
    NSMutableArray *properties = [NSMutableArray array];
    
    NSAttributeDescription *remoteURLAttribute = [[NSAttributeDescription alloc] init];
    [remoteURLAttribute setName:@"remoteURL"];
    [remoteURLAttribute setAttributeType:NSStringAttributeType];
    [remoteURLAttribute setOptional:NO];
    [remoteURLAttribute setIndexed:YES];
    [properties addObject:remoteURLAttribute];
    
    //    NSAttributeDescription *fileDataAttribute = [[NSAttributeDescription alloc] init];
    //    [fileDataAttribute setName:@"fileData"];
    //    [fileDataAttribute setAttributeType:NSBinaryDataAttributeType];
    //    [fileDataAttribute setOptional:NO];
    //    [fileDataAttribute setAllowsExternalBinaryDataStorage:YES];
    //    [properties addObject:fileDataAttribute];
    //
    //    NSAttributeDescription *lastAccessDateAttribute = [[NSAttributeDescription alloc] init];
    //    [lastAccessDateAttribute setName:@"lastAccessDate"];
    //    [lastAccessDateAttribute setAttributeType:NSDateAttributeType];
    //    [lastAccessDateAttribute setOptional:NO];
    //    [properties addObject:lastAccessDateAttribute];
    //
    //    NSAttributeDescription *expirationDateAttribute = [[NSAttributeDescription alloc] init];
    //    [expirationDateAttribute setName:@"expirationDate"];
    //    [expirationDateAttribute setAttributeType:NSDateAttributeType];
    //    [expirationDateAttribute setOptional:NO];
    //    [properties addObject:expirationDateAttribute];
    //
    //    NSAttributeDescription *contentTypeAttribute = [[NSAttributeDescription alloc] init];
    //    [contentTypeAttribute setName:@"contentType"];
    //    [contentTypeAttribute setAttributeType:NSStringAttributeType];
    //    [contentTypeAttribute setOptional:YES];
    //    [properties addObject:contentTypeAttribute];
    //
    //    NSAttributeDescription *fileSizeAttribute = [[NSAttributeDescription alloc] init];
    //    [fileSizeAttribute setName:@"fileSize"];
    //    [fileSizeAttribute setAttributeType:NSInteger32AttributeType];
    //    [fileSizeAttribute setOptional:NO];
    //    [properties addObject:fileSizeAttribute];
    //
    //    NSAttributeDescription *entityTagIdentifierAttribute = [[NSAttributeDescription alloc] init];
    //    [entityTagIdentifierAttribute setName:@"entityTagIdentifier"];
    //    [entityTagIdentifierAttribute setAttributeType:NSStringAttributeType];
    //    [entityTagIdentifierAttribute setOptional:YES];
    //    [properties addObject:entityTagIdentifierAttribute];
    
    // add attributes to entity
    [entity setProperties:properties];
    
    // add entity to model
    [model setEntities:[NSArray arrayWithObject:entity]];
    
    return model;
}


+ (void) deleteAllFavImagesAndTheirRecordsFromCoreDataOnLogout
{
    @try
    {
        [AppDelegate appDelegate].myFavouritePhotosVCInstance = nil;

        NSArray * dbResultSetArray = [CoreDataHelper getObjectsList:@"CachedImages"];
        for (NSManagedObject * nsManagedObject  in dbResultSetArray)
        {
            [BasicUtilities removeImageFromDocumentDirectory:[nsManagedObject valueForKey:@"imageName"]]; //   delete image from document directory
//            [BasicUtilities removeImageFromDocumentDirectory:[NSString stringWithFormat:@"NotFav%@",[nsManagedObject valueForKey:@"imageName"]]]; //   delete image from document directory
            [CoreDataHelper deleteManagedObject:nsManagedObject]; // delete Favourite object from coredata
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@",exception);
    }
}

//
//if ([[CoreDataHelper findContact:@"imageName" value:[NSString stringWithFormat:@"NotFav%@",[[photoDetailResponseDict objectForKey:KPhotoImageDetail] objectForKey:KActionkey]] EntityName:@"CachedImages"] count] > 0)
//{
//    [cacheImageBtn setImage:[UIImage imageNamed:@"download.png"] forState:UIControlStateNormal];
//}
//else
//{
//    [cacheImageBtn setImage:[UIImage imageNamed:@"downloadIcon.png"] forState:UIControlStateNormal];
//}



+ (void) deleteCachedImagesAndItsRecordsFromCoreDataAndDD:(NSString *)fileName
{
    @try
    {
        [AppDelegate appDelegate].myFavouritePhotosVCInstance = nil;
        
        if ([[CoreDataHelper findContact:@"imageName" value:fileName EntityName:@"CachedImages"] count] > 0)
        {
            NSArray * dbResultSetArray = [CoreDataHelper findContact:@"imageName" value:fileName EntityName:@"CachedImages"];
            for (NSManagedObject * nsManagedObject  in dbResultSetArray)
            {
                [CoreDataHelper deleteManagedObject:nsManagedObject]; // delete Favourite object from coredata
                [BasicUtilities removeImageFromDocumentDirectory:[NSString stringWithFormat:@"%@",[nsManagedObject valueForKey:@"imageName"]]]; //   delete image from document directory
            }
        }
        


    }
    @catch (NSException *exception)
    {
        NSLog(@"%@",exception);
    }
}



@end
