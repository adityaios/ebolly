//
//  DBTables.h
//  FlrtAlert
//
//  Created by CraterZone on 03/04/14.
//  Copyright (c) 2014 Craterzone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SQLiteHelper.h"

@interface DBTables : NSObject

-(void)createDBTables;

@end
