//
//  DBTables.m
//  FlrtAlert
//
//  Created by CraterZone on 03/04/14.
//  Copyright (c) 2014 Craterzone. All rights reserved.
//

#import "DBTables.h"
#import "NetworkConstants.h"

@implementation DBTables {
    sqlite3 *database;
}

-(void)createDBTables
{
    SQLiteHelper *sqliteHelper =[SQLiteHelper sharedSingleton];
    database =[sqliteHelper getDataBase];
    [self createFocvsCacheTable];

}

-(void)createFocvsCacheTable {
    char *errMsg;
    NSString *sql = [NSString stringWithFormat:@"create table if not exists %@ (action text, response text)",KFocvsCacheTable];
    // NSLog(@"%@",sql_stmt);
    if (sqlite3_exec(database, [sql UTF8String], NULL, NULL, &errMsg) != SQLITE_OK)
    {
        NSLog(@"Failed To Create FocvsCache Table");
    }
    else
    {
        NSLog(@"Successfully Created FocvsCache Table");
    }
}



//-(void)createUserTable {
//    char *errMsg;
//    NSString *sql = @"create table if not exists user (userId integer primary key, name text, gender text, dob text, height integer, weight integer, ethnicity text,thumbnailURL text ,aboutMe text,status text)";
//    // NSLog(@"%@",sql_stmt);
//    if (sqlite3_exec(database, [sql UTF8String], NULL, NULL, &errMsg) != SQLITE_OK)
//    {
//        NSLog(@"Failed to create table");
//    }
//    else
//    {
//        NSLog(@"successfully created user table");
//    }
//}
//
//-(void)createFilterTable {
//    char *errMsg;
//    NSString *sql = @"create table if not exists filter (userId integer primary key, ageMax integer, ageMin integer, gender text, weightMin integer, weightMax integer, heightMax integer, ethnicity text, heightMin integer, proximity text)";
//    // NSLog(@"%@",sql_stmt);
//    if (sqlite3_exec(database, [sql UTF8String], NULL, NULL, &errMsg) != SQLITE_OK)
//    {
//        NSLog(@"Failed to create table");
//    }
//    else
//    {
//        NSLog(@"successfully created Filter table");
//    }
//}
//

@end
