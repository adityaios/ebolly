//
//  SQLiteHelper.h
//  StockMarketApp
//
//  Created by CraterZone on 22/01/14.
//  Copyright (c) 2014 Craterzone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface SQLiteHelper : NSObject {
    
}

- (void)initializeTheDatabase;
- (NSString *)databasePath;
- (void)openDatabase:(NSString *)databasePath;
+ (void)finalizeStatement :(sqlite3_stmt *)stmt;
+ (void)closeDatabase;
- (void)finalizeAndClose :(sqlite3_stmt *)stmt;
+ (SQLiteHelper *)sharedSingleton;
-(sqlite3 *)getDataBase;
@end
