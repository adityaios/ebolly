//
//  SQLiteHelper.m
//  StockMarketApp
//
//  Created by CraterZone on 22/01/14.
//  Copyright (c) 2014 Craterzone. All rights reserved.
//

#import "SQLiteHelper.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define DATABASE_NAME @"Focvs.sqlite"
#define DATABASE_TITLE @"Focvs"
#define DATABASE_TYPE @"sqlite"

@interface SQLiteHelper()

{
    sqlite3 *_database;
    
}

// database initializations


// data base operations-


@end

@implementation SQLiteHelper

- (id)init
{
    self=[super init];
    
    if(self)
    {
        // call necessary function
        [self createEditableCopyOfDatabaseIfNeeded];
    }
    return self;
}

/*
 This Method creates a Singleton object of our database.
 return : if database already exists then return that DB else create new DB object
 */
+ (SQLiteHelper *)sharedSingleton
{
    static SQLiteHelper *sharedSingleton;
    
    @synchronized(self)
    {
        if (!sharedSingleton)
            sharedSingleton = [[SQLiteHelper alloc] init];
            return sharedSingleton;
    }
}


-(sqlite3 *)getDataBase {
    return _database;
}

#pragma mark - DataBase(opening/closing)
- (void)initializeTheDatabase
{
    // get the file path and get the file manager and
    // check if the file exists at the path if yes then dont create
    // else create a new file and open the database
    NSString *documentDatabasePath = [self databasePath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    if([fileManager fileExistsAtPath:documentDatabasePath])
    {
        NSLog(@"file exists here");
        [self openDatabase:documentDatabasePath];
    }
    
    else
    {
        NSLog(@"file does not exist here");
        // file does not exist then copy the file if it exists in the
        // local bundle or else create a new database and open the
        // database that is copied at the document directory
        if([fileManager  copyItemAtPath:[[NSBundle mainBundle]pathForResource:DATABASE_TITLE ofType:DATABASE_TYPE] toPath:documentDatabasePath error:&error])
        {
            NSLog(@"Copied the data base successfully");
            [self openDatabase:documentDatabasePath];
        }
        else
        {
            NSLog(@"Cannot copy the database error: %@",[error localizedDescription]);
        }
    }
}

/*
 This method creates Editable copy of database if not exists 
 at resource bundle path.
 */
-(void)createEditableCopyOfDatabaseIfNeeded
{
	BOOL success;
	NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:DATABASE_NAME];
	
	CGFloat currentVersion = [[NSUserDefaults standardUserDefaults] floatForKey:@"currentVersion"];
	
    CGFloat newVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)@"CFBundleVersion"] floatValue];
    
	if(currentVersion == 0 || currentVersion < newVersion) {
        [self openDatabase:writableDBPath];
        
		[[NSUserDefaults standardUserDefaults] setFloat:newVersion forKey:@"currentVersion"];
	}
	else
    {
		success = [fileManager fileExistsAtPath:writableDBPath];
		if (success)
        {
            NSLog(@"success");
            [self openDatabase:writableDBPath];
            return;
        }
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DATABASE_NAME];
		
		   success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
           [self openDatabase:writableDBPath];
		if (!success) {
			NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
		}
	}
}

/*
 This Method returns path of our app database file.
 return : NSString(it contains database path).
 */
- (NSString *)databasePath
{
    // first get the path for the document directory of the application
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dbpath = [paths objectAtIndex:0];
    
    // append the database to the path of the the document directory path
    return [dbpath stringByAppendingPathComponent:DATABASE_NAME];
}

/*
 Open database connection
 params : NSString(it contains database path).
 */
- (void)openDatabase:(NSString *)databasePath
{
    // the path of the database should be in the form of the UTF8 encoding
    // because of the compatibility of the Character set
    const char *dbPath = [databasePath UTF8String];
    
    if(sqlite3_open(dbPath, &_database)==SQLITE_OK)
    {
        // the database has opened successfully
        NSLog(@"database is open");
    }
    else
    {
        // the database is not opened due to some error
        NSLog(@"database is not open");
    }
    
}

/*
 Finalize statement after execution.
 params : sqlite3_stmt
 */
+ (void)finalizeStatement :(sqlite3_stmt *)stmt
{
    // finalize the statements
    if(sqlite3_finalize(stmt) == SQLITE_OK)
    {
        NSLog(@"finalized the statement successfully");
    }
}

/*
 Finalize statement and close database connection after execution.
 params : sqlite3_stmt
 */
- (void)finalizeAndClose :(sqlite3_stmt *)stmt
{
    [SQLiteHelper finalizeStatement: stmt];
    [SQLiteHelper closeDatabase];
}

/*
 close database connection if open
 */
+ (void)closeDatabase
{
    // close the database
    if(sqlite3_close((__bridge sqlite3 *)([SQLiteHelper sharedSingleton])) ==SQLITE_OK)
    {
        NSLog(@"databse successfully closed");
    }
}



@end
