//
//  UserDao.h
//  FlrtAlert
//
//  Created by CraterZone on 03/04/14.
//  Copyright (c) 2014 Craterzone. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "SQLiteHelper.h"
#import "DBTables.h"

enum errorCodes {
    kDBNotExists,
    kDBFailAtOpen,
    kDBFailAtCreate,
    kDBErrorQuery,
    kDBFailAtClose
};

@interface UserDao : NSObject {
    SQLiteHelper *sqliteHelper;
    
}
- (BOOL)insertFocvsCacheDetails: (NSDictionary *)JsonWithAction; // Focvs

- (void)insertInDB: (NSDictionary *)JsonWithAction error:(NSError**)error;
- (void)updateTable: (NSDictionary *)JsonWithAction error:(NSError**)error;
- (int)rowCounter:(NSString *)tableName ColumnName:(NSString *)columnName error:(NSError**)error;
- (NSError *) closeDatabase;
- (NSMutableArray *)getRowsForQuery:(NSString *)sql;
- (NSError *) openDatabase;
-(void)deleteRows:(NSString *)tableName;
@end
