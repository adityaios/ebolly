//
//  UserDao.m
//  FlrtAlert
//
//  Created by CraterZone on 03/04/14.
//  Copyright (c) 2014 Craterzone. All rights reserved.
//

#import "UserDao.h"
#import "NetworkConstants.h"
@implementation UserDao {
    
    sqlite3 *database;
}

#define KDatabaseName @"FocvsCache"

/******************************************************Example Query String*******************************************************************/
//        [userDao insertInDB:[NSDictionary dictionaryWithObjectsAndKeys:[userInfo objectForKey:KActionkey],kAction,response,@"response", nil]error:nil];
//        [userDao closeDatabase];
//        [userDao getRowsForQuery:@"select * from FocvsCache where action = 'myPhotos'"];
//         [userDao executeQuery:@"delete from FocvsCache"];
//        [userDao deleteRows:@"FocvsCache"];
//        [userDao updateTable:[NSDictionary dictionaryWithObjectsAndKeys:[userInfo objectForKey:KActionkey],kAction,@"rahul",@"response", nil] error:nil];
//        [userDao rowCounter:@"FocvsCache" error:nil];
/***********************************************************************************************************************************************/

- (void)insertInDB: (NSDictionary *)JsonWithAction error:(NSError**)error
{
    if (![self ensureDatabaseOpen:error])
    {
        return;
    }
    @try {
       
    sqliteHelper = [SQLiteHelper sharedSingleton];
    database = [sqliteHelper getDataBase];
    NSLog(@">> ContactManager::insertInFocvsCache");
    
    // prep statement
    sqlite3_stmt    *statement;
    NSString *querySQL = @"INSERT INTO FocvsCache (action, response) VALUES(?,?)";
    NSLog(@"query: %@", querySQL);
    const char *query_stmt = [querySQL UTF8String];
    
    // preparing a query compiles the query so it can be re-used.
    sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
    sqlite3_bind_text(statement, 1, [[JsonWithAction objectForKey:@"action"] UTF8String], -1, SQLITE_STATIC);
    sqlite3_bind_text(statement, 2, [[JsonWithAction objectForKey:@"response"] UTF8String] , -1, SQLITE_TRANSIENT);
//    sqlite3_bind_text(statement, 2, [[NSString stringWithFormat:@"%@",[JsonWithAction objectForKey:@"response"]] UTF8String] , -1, SQLITE_TRANSIENT);
//    sqlite3_bind_blob(statement, 2, (__bridge const void *)([JsonWithAction objectForKey:@"response"]), -1, SQLITE_TRANSIENT);
        
    // process result
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog(@"error: %s", sqlite3_errmsg(database));
    }
    
    sqlite3_finalize(statement);
    }
    @catch (NSException *exception)
    {
        NSLog(exception);
    }

}

- (int)rowCounter:(NSString *)tableName ColumnName:(NSString *)columnName error:(NSError**)error
{
    int  numrows;
    if (![self ensureDatabaseOpen:error])
    {
        return 0;
    }
    @try {
        
        sqliteHelper = [SQLiteHelper sharedSingleton];
        database = [sqliteHelper getDataBase];
        NSLog(@">> ContactManager::RowCounter");
        
        // prep statement
//        sqlite3_stmt    *statement;
        if(sqlite3_open([[sqliteHelper databasePath] UTF8String], &database) == SQLITE_OK)
        {
            NSString *sql = [NSString stringWithFormat:@"select count(*) from %@ where action = '%@'", tableName, columnName];
            sqlite3_stmt *selectStatement;
            const char *query_stmt = [sql UTF8String];
            
            int returnValue = sqlite3_prepare_v2(database, query_stmt, -1, &selectStatement, NULL);
            if (returnValue == SQLITE_OK)
            {
                if(sqlite3_step(selectStatement) == SQLITE_ROW)
                {
                    numrows= sqlite3_column_int(selectStatement, 0);
                }
            }
            sqlite3_finalize(selectStatement);
            sqlite3_close(database);
        }
        
    }
    @catch (NSException *exception)
    {
        NSLog(exception);
    }
    return numrows;
}



- (void)updateTable: (NSDictionary *)JsonWithAction error:(NSError**)error
{
    if (![self ensureDatabaseOpen:error])
    {
        return;
    }
    @try {
        
        sqliteHelper = [SQLiteHelper sharedSingleton];
        database = [sqliteHelper getDataBase];
        NSLog(@">> ContactManager::updateFocvsCache");
        
        // prep statement
        sqlite3_stmt    *statement;
        NSString *sqlQuery = @"UPDATE FocvsCache SET response = ? WHERE action = ?";
        const char *query_stmt = [sqlQuery UTF8String];
        
        if (query_stmt == nil)
        {
            return;
        }
        
        // preparing a query compiles the query so it can be re-used.
        sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
//        sqlite3_bind_text(statement, 1, [[JsonWithAction objectForKey:@"action"] UTF8String], -1, SQLITE_STATIC);
        sqlite3_bind_text(statement, 1, [[NSString stringWithFormat:@"%@",[JsonWithAction objectForKey:@"response"]] UTF8String] , -1, SQLITE_STATIC);
        sqlite3_bind_text(statement, 2, [[JsonWithAction objectForKey:@"action"] UTF8String], -1, SQLITE_STATIC);
        
        // process result
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog(@"error: %s", sqlite3_errmsg(database));
        }
        
        sqlite3_finalize(statement);
    }
    @catch (NSException *exception)
    {
        NSLog(exception);
    }
    
}



- (BOOL)ensureDatabaseOpen: (NSError **)error
{
    // already created db connection
    sqliteHelper =[SQLiteHelper sharedSingleton];
    if (database != nil)
    {
        [sqliteHelper openDatabase:[sqliteHelper databasePath]];
        return YES;
    }

    NSLog(@">> ContactManager::ensureDatabaseOpen");
    if (![self ensureDatabasePrepared:error])
    {
        return NO;
    }
    
    const char *dbpath = [[sqliteHelper databasePath] UTF8String];
    if (sqlite3_open(dbpath, &database) != SQLITE_OK &&
        error != nil)
    {
        *error = [[NSError alloc] initWithDomain:@"ContactsManager" code:1000 userInfo:nil];
        return NO;
    }
    
    NSLog(@"opened");
    
    return YES;
}

- (BOOL)ensureDatabasePrepared: (NSError **)error
{
     sqliteHelper =[SQLiteHelper sharedSingleton];
    // already prepared
    if (([sqliteHelper databasePath] != nil) &&
        ([[NSFileManager defaultManager] fileExistsAtPath:[sqliteHelper databasePath]]))
    {
        return YES;
    }
    
    // db in main bundle - cant edit.  copy to library if !exist
    NSString *dbTemplatePath = [[NSBundle mainBundle] pathForResource:KDatabaseName ofType:@"sqlite"];
    NSLog(@"%@", dbTemplatePath);
    
    NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
    NSString * _dbPath = [libraryPath stringByAppendingPathComponent:@"FocvsCache.sqlite"];
    
    NSLog(@"dbPath: %@", _dbPath);
    
    // copy db from template to library
    if (![[NSFileManager defaultManager] fileExistsAtPath:_dbPath])
    {
        NSLog(@"db not exists");
        NSError *error = nil;
        if (![[NSFileManager defaultManager] copyItemAtPath:dbTemplatePath toPath:_dbPath error:&error])
        {
            return NO;
        }
        
        NSLog(@"copied");
    }    
    
    return YES;    
}




- (NSMutableArray *)getRowsForQuery:(NSString *)sql {
    
    NSMutableArray *resultsArray = [[NSMutableArray alloc] initWithCapacity:1];
    
    

        sqliteHelper = [SQLiteHelper sharedSingleton];
        database = [sqliteHelper getDataBase];
        NSLog(@">> ContactManager::getResultSetFocvsCache");
    NSError * err;
    if (![self ensureDatabaseOpen:&err])
    {
        return nil;
    }
        // prep statement
        sqlite3_stmt    *statement;

    const char *query = [sql UTF8String];
    int returnCode = sqlite3_prepare_v2(database, query, -1, &statement, NULL);
    
    if (returnCode == SQLITE_ERROR) {
        const char *errorMsg = sqlite3_errmsg(database);
        NSError *errorQuery = [self createDBErrorWithDescription:[NSString stringWithCString:errorMsg encoding:NSUTF8StringEncoding]
                                                         andCode:kDBErrorQuery];
        NSLog(@"%@", errorQuery);
    }
    
    while (sqlite3_step(statement) == SQLITE_ROW) {
        int columns = sqlite3_column_count(statement);
        NSMutableDictionary *result = [[NSMutableDictionary alloc] initWithCapacity:columns];
        
        for (int i = 0; i<columns; i++) {
            const char *name = sqlite3_column_name(statement, i);
            
            NSString *columnName = [NSString stringWithCString:name encoding:NSUTF8StringEncoding];
            
            int type = sqlite3_column_type(statement, i);
            
            switch (type) {
                case SQLITE_INTEGER:
                {
                    int value = sqlite3_column_int(statement, i);
                    [result setObject:[NSNumber numberWithInt:value] forKey:columnName];
                    break;
                }
                case SQLITE_FLOAT:
                {
                    float value = sqlite3_column_double(statement, i);
                    [result setObject:[NSNumber numberWithFloat:value] forKey:columnName];
                    break;
                }
                case SQLITE_TEXT:
                {
                    const char *value = (const char*)sqlite3_column_text(statement, i);
                    [result setObject:[NSString stringWithCString:value encoding:NSUTF8StringEncoding] forKey:columnName];
                    break;
                }
                    
                case SQLITE_BLOB:
                {
                    int bytes = sqlite3_column_bytes(statement, i);
                    if (bytes > 0) {
                        const void *blob = sqlite3_column_blob(statement, i);
                        if (blob != NULL) {
                            [result setObject:[NSData dataWithBytes:blob length:bytes] forKey:columnName];
                        }
                    }
                    break;
                }
                    
                case SQLITE_NULL:
                    [result setObject:[NSNull null] forKey:columnName];
                    break;
                    
                default:
                {
                    const char *value = (const char *)sqlite3_column_text(statement, i);
                    [result setObject:[NSString stringWithCString:value encoding:NSUTF8StringEncoding] forKey:columnName];
                    break;
                }
                    
            } //end switch
            
            
        } //end for
        
        [resultsArray addObject:result];
        
    } //end while
    sqlite3_finalize(statement);
    
    [self closeDatabase];
    
    return resultsArray;
    
}

- (NSError *) openDatabase {
    
    NSError *error = nil;
    
    NSString *databasePath = [[SQLiteHelper sharedSingleton] databasePath];
    
    const char *dbpath = [databasePath UTF8String];
    int result = sqlite3_open(dbpath, &database);
    if (result != SQLITE_OK) {
        const char *errorMsg = sqlite3_errmsg(database);
        NSString *errorStr = [NSString stringWithFormat:@"The database could not be opened: %@",[NSString stringWithCString:errorMsg encoding:NSUTF8StringEncoding]];
        error = [self createDBErrorWithDescription:errorStr	andCode:kDBFailAtOpen];
    }
    
    return error;
}


- (NSError *) closeDatabase
{
    NSError *error = nil;
    
    
    if (database != nil) {
        if (sqlite3_close(database) != SQLITE_OK){
            const char *errorMsg = sqlite3_errmsg(database);
            NSString *errorStr = [NSString stringWithFormat:@"The database could not be closed: %@",[NSString stringWithCString:errorMsg encoding:NSUTF8StringEncoding]];
            error = [self createDBErrorWithDescription:errorStr andCode:kDBFailAtClose];
        }
        
        database = nil;
    }
    return error;
}

- (NSError *)createDBErrorWithDescription:(NSString*)description andCode:(int)code {
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:description, NSLocalizedDescriptionKey, nil];
    NSError *error = [NSError errorWithDomain:@"SQLite Error" code:code userInfo:userInfo];
    
    return error;
}



-(void)deleteRows:(NSString *)tableName
{
    if (![self ensureDatabaseOpen:nil])
    {
        return;
    }
    @try {
        
        sqliteHelper = [SQLiteHelper sharedSingleton];
        database = [sqliteHelper getDataBase];
        NSLog(@">> ContactManager::deleteRows");
        
        // prep statement
        sqlite3_stmt    *statement;
        NSString *querySQL = [NSString stringWithFormat:@"delete from %@",tableName];
        NSLog(@"query: %@", querySQL);
        const char *query_stmt = [querySQL UTF8String];
        
        // preparing a query compiles the query so it can be re-used.
        sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
        
        // process result
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog(@"error: %s", sqlite3_errmsg(database));
        }
        
        sqlite3_finalize(statement);
    }
    @catch (NSException *exception)
    {
        NSLog(exception);
    }
}

































///*
// This method Inserts Data in user Table
// */
//- (BOOL)insertUser: (User *)user
//{
//    if (user == nil)
//    {
//        return NO;
//    }
//    sqlite3_stmt *init_statement=nil;
//    int success;
//    @try {
//        SQLiteHelper *sqliteHelper =[SQLiteHelper sharedSingleton];
//        if (sqliteHelper != nil)
//        {
//            database = [sqliteHelper getDataBase];
//            if (database == nil)
//            {
//                return NO;
//            }
//        }
//        else
//        {
//            return NO;
//        }
//
//        NSString *str = @"INSERT INTO User (userId, name, gender, dob, height, weight, ethnicity, thumbnailURL,aboutMe,status) VALUES(?,?,?,?,?,?,?,?,?,?)";
//		
//        const char *sql = [str UTF8String];
//		if (sql == nil)
//        {
//            return NO;
//        }
//        
//        if (sqlite3_prepare_v2(database, sql, -1, &init_statement, NULL) != SQLITE_OK) {
//            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
//            return NO;
//        }
//         
//        
//        NSString * timeStampString = user.dob;
//        NSTimeInterval interval=[timeStampString doubleValue]/1000;
//        NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
//        user.dob = [dateFormatter stringFromDate:date];
//        
//        user.aboutMe = @"About me";
//        sqlite3_bind_int(init_statement, 1, user.userId);
//        sqlite3_bind_text(init_statement, 2, [user.name UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(init_statement, 3, [user.gender UTF8String] , -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(init_statement, 4, [user.dob UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_int(init_statement, 5, user.height);
//        sqlite3_bind_int(init_statement, 6, user.weight);
//        sqlite3_bind_text(init_statement, 7, [user.ethnicity UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(init_statement, 8,[user.thumbnailURL UTF8String],-1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(init_statement, 9,[user.aboutMe UTF8String],-1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(init_statement, 10,[user.status UTF8String],-1,SQLITE_TRANSIENT);
//        success = sqlite3_step(init_statement);
//        
//        if (success == SQLITE_ERROR) {
//            NSAssert1(0, @"Error: failed to insert user Data into the database with message '%s'.", sqlite3_errmsg(database));
//            return NO;
//        }
//    }
//    @catch (NSException *exception) {
//        if (success == SQLITE_ERROR) {
//            NSAssert1(0, @"Error: failed to insert user Data into the database with message '%s'.", sqlite3_errmsg(database));
//            return NO;
//        }
//        sqlite3_finalize(init_statement);
//    }
//    @finally {
//        if (success == SQLITE_ERROR) {
//            NSAssert1(0, @"Error: failed to insert user Data into the database with message '%s'.", sqlite3_errmsg(database));
//            return NO;
//        }
//        sqlite3_finalize(init_statement);
//    }
//	return YES;
//}
//
//- (BOOL)insertUserFilterDetails: (Filter *)filter
//{
//    if (filter == nil)
//    {
//        return NO;
//    }
//    
//    sqlite3_stmt *init_statement=nil;
//    int success;
//    @try {
//        SQLiteHelper *sqliteHelper =[SQLiteHelper sharedSingleton];
//        if (sqliteHelper != nil)
//        {
//            database = [sqliteHelper getDataBase];
//            if (database == nil)
//            {
//                return NO;
//            }
//        }
//        else
//        {
//            return NO;
//        }
//        NSString *str = @"INSERT INTO filter (userId, gender, ethnicity, proximity, heightMin, heightMax, weightMin, weightMax, ageMin, ageMax) VALUES(?,?,?,?,?,?,?,?,?,?)";
//		
//        const char *sql = [str UTF8String];
//		if (sql == nil)
//        {
//            return NO;
//        }
//        
//        if (sqlite3_prepare_v2(database, sql, -1, &init_statement, NULL) != SQLITE_OK) {
//            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
//            return NO;
//        }
//        
//        filter.ethnicity = @"";
//        filter.proximity = @"";
//        sqlite3_bind_int(init_statement, 1, filter.userId);
//        sqlite3_bind_text(init_statement, 2, [filter.gender UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(init_statement, 3, [filter.ethnicity UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(init_statement, 4, [filter.proximity UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_int(init_statement, 5,  filter.heightMin);
//        sqlite3_bind_int(init_statement, 6,  filter.heightMax);
//        sqlite3_bind_int(init_statement, 7,  filter.weightMin);
//        sqlite3_bind_int(init_statement, 8,  filter.weightMax);
//        sqlite3_bind_int(init_statement, 9,  filter.ageMin);
//        sqlite3_bind_int(init_statement, 10,  filter.ageMax);
//
//        success = sqlite3_step(init_statement);
//        
//        if (success == SQLITE_ERROR) {
//            NSAssert1(0, @"Error: failed to insert user Data into the database with message '%s'.", sqlite3_errmsg(database));
//            return NO;
//        }
//    }
//    @catch (NSException *exception) {
//        if (success == SQLITE_ERROR) {
//            NSAssert1(0, @"Error: failed to insert user Data into the database with message '%s'.", sqlite3_errmsg(database));
//            return NO;
//        }
//        sqlite3_finalize(init_statement);
//    }
//    @finally {
//        if (success == SQLITE_ERROR) {
//            NSAssert1(0, @"Error: failed to insert user Data into the database with message '%s'.", sqlite3_errmsg(database));
//            return NO;
//        }
//        sqlite3_finalize(init_statement);
//    }
//	return YES;
//}
//
//
//- (void)updateUserFilterData:(Filter *)filter{
//    if (filter == nil)
//    {
//        return;
//    }
//    
//    sqlite3_stmt *init_statement = nil;
//    int success;
//    @try {
//        SQLiteHelper *sqliteHelper =[SQLiteHelper sharedSingleton];
//        if (sqliteHelper != nil)
//        {
//            database = [sqliteHelper getDataBase];
//            if (database == nil)
//            {
//                return;
//            }
//        }
//        else
//        {
//            return;
//        }
//
//        NSString *sqlQuery = @"UPDATE filter SET userID = ?,gender = ?,ethnicity = ?,proximity = ?,heightMin = ?,heightMax = ?,weightMin = ?,weightMax = ?,ageMin = ?,ageMax = ?";
//        
//        const char *sql = [sqlQuery UTF8String];
//        if (sql == nil)
//        {
//            return;
//        }
//        if (sqlite3_prepare_v2(database, sql, -1, &init_statement, NULL) != SQLITE_OK) {
//            
//            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
//            
//        }
//
//        sqlite3_bind_int(init_statement, 1, filter.userId);
//        sqlite3_bind_text(init_statement, 2, [filter.gender UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(init_statement, 3, [filter.ethnicity UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(init_statement, 4, [filter.proximity UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_int(init_statement, 5,  filter.heightMin);
//        sqlite3_bind_int(init_statement, 6,  filter.heightMax);
//        sqlite3_bind_int(init_statement, 7,  filter.weightMin);
//        sqlite3_bind_int(init_statement, 8,  filter.weightMax);
//        sqlite3_bind_int(init_statement, 9,  filter.ageMin);
//        sqlite3_bind_int(init_statement, 10,  filter.ageMax);
//        success = sqlite3_step(init_statement);
//        
//        if (success == SQLITE_ERROR) {
//            
//            NSAssert1(0, @"Error: failed to update suggested friends Data into the database with message '%s'.", sqlite3_errmsg(database));
//            
//        }
//        
//    }
//    
//    @catch (NSException *exception) {
//        
//        if (success == SQLITE_ERROR) {
//            
//            NSAssert1(0, @"Error: failed to update User Data into the database '%s'.", sqlite3_errmsg(database));
//            
//            sqlite3_finalize(init_statement);
//        }
//    }
//    @finally {
//        if(success == SQLITE_ERROR) {
//            
//            NSAssert1(0, @"Error: failed to update User Data into the database '%s'.", sqlite3_errmsg(database));
//            
//        }
//        
//        sqlite3_finalize(init_statement);
//    }
//    
//}
//
//
//
///*
// To retrieve userHash from user Table
// */
//
//- (NSString *)getUsername {
//    
//    NSString *username;
//    
//    NSString *sqlQuery = @"SELECT name FROM User";
//    
//    username = [self getStringFromDB:sqlQuery];
//    
//    if(username != nil && [username length] > 0) {
//        
//        return  username ;
//        
//    }
//    
//    else {
//        
//        username = DEFAULT_STRING_VALUE;
//        
//    }
//    
//    return username;
//    
//}
//
//
//
///*
// 
// To retrieve mobileNumber of user from User Table
// 
// */
//
//- (NSString *)getPassword {
//    
//    NSString *password;
//    
//    NSString *sqlQuery = @"SELECT password FROM User";
//    
//    password = [self getStringFromDB:sqlQuery];
//    
//    if(password != nil && [password length] >= 6) {
//        
//        return  password ;
//        
//    }
//    
//    else {
//        
//        password = DEFAULT_STRING_VALUE;
//        
//    }
//    
//    return password;
//    
//}
//
///*
// To retrieve userId from user Table
// */
//
//- (NSString *)getUserId {
//    
//    NSString *userId;
//    
//    NSString *sqlQuery = @"SELECT userId FROM User";
//    
//    userId = [self getStringFromDB:sqlQuery];
//    
//    if(userId != nil && [userId length] > 0) {
//        
//        return  userId ;
//        
//    }
//    
//    else {
//        
//        userId = DEFAULT_STRING_VALUE;
//        
//    }
//    
//    return userId;
//    
//}
//
//
//
///*
// 
// To get single string from User Table
// 
// */
//
//-(NSString *)getStringFromDB:(NSString *)sqlQuery
//{
//    if (sqlQuery != nil)
//    {
//        sqlite3_stmt *init_statement = NULL;
//        NSString *result;
//        @try {
//        
//            SQLiteHelper *sqliteHelper =[SQLiteHelper sharedSingleton];
//            if (sqliteHelper != nil)
//            {
//                database = [sqliteHelper getDataBase];
//                if (database == nil)
//                {
//                    return NO;
//                }
//            }
//            else
//            {
//                return NO;
//            }
//
//
//            const char *sql = [sqlQuery UTF8String];
//            if (sql == nil)
//            {
//                return NO;
//            }
//                if (sqlite3_prepare_v2(database, sql, -1, &init_statement, NULL) == SQLITE_OK) {
//            
//                    while (sqlite3_step(init_statement) == SQLITE_ROW) {
//                
//                        result = [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(init_statement, 0)];
//            
//                    }
//            
//                }
//        
//        }
//    
//        @catch (NSException *exception) {
//        
//            NSLog(@"Error Occured while Retrieving userHash");
//        
//        }
//    
//        @finally {
//        
//            sqlite3_finalize(init_statement);
//            
//        }
//    
//    return result;
//    }
//}
//
//
//-(User *)getUserProfileDetailFromDB  {
//    
//    User *user = [[User alloc ] init] ;
//    sqlite3_stmt *init_statement = nil;
//    @try {
//        SQLiteHelper *sqliteHelper =[SQLiteHelper sharedSingleton];
//        if (sqliteHelper != nil)
//        {
//            database = [sqliteHelper getDataBase];
//            if (database == nil)
//            {
//                return NO;
//            }
//        }
//        else
//        {
//            return NO;
//        }
//
//        const char * sql = "SELECT name,gender,dob,height,weight,ethnicity,thumbnailURL,aboutMe FROM User";
//        if (sqlite3_prepare_v2(database, sql, -1, &init_statement, NULL) == SQLITE_OK) {
//            
//            while (sqlite3_step(init_statement) == SQLITE_ROW) {
//                
//               user.name = [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(init_statement, 0)];
//                
//               user.gender = [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(init_statement, 1)];
//                
//               user.dob = [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(init_statement, 2)];
//             
//                user.height = sqlite3_column_int(init_statement,3);
//                
//                user.weight = sqlite3_column_int(init_statement,4);
//                
//             user.ethnicity = [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(init_statement, 5)];
//                
//               user.thumbnailURL = [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(init_statement, 6)];
//                
//                const unsigned char *str = sqlite3_column_text(init_statement, 7);
//                if (str != nil ) {
//                    user.aboutMe = [[NSString alloc] initWithUTF8String:(char *) str];
//                } else {
//                    NSLog(@"Failed to get user data '%s'.",sqlite3_errmsg(database));
//                }
//            
//            
//            }
//            
//        }
//      
//    }
//    
//    @catch (NSException *exception) {
//        
//        NSAssert1(0,@"Error: Failed to get user data '%s'.",sqlite3_errmsg(database));
//        
//        sqlite3_finalize(init_statement);
//        
//        sqlite3_close(database);
//        
//    }
//    
//    @finally {
//        
//        sqlite3_finalize(init_statement);
//        
//      //  [sqliteHelper closeDB];
//        
//    }
//    
//     return user;
//    
//}
//
///*
// 
// Update suggested friends data in user table
// 
// */
//
//- (void)updateUserData:(User *)user{
//    
//    if (user == nil)
//    {
//        return;
//    }
//    sqlite3_stmt *init_statement = nil;
//    int success;
//    @try {
//        SQLiteHelper *sqliteHelper =[SQLiteHelper sharedSingleton];
//        database = [sqliteHelper getDataBase];
//        NSString * timeStampString = user.dob;
//        NSTimeInterval _interval=[timeStampString doubleValue];
//        NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
//        NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
//        [_formatter setDateFormat:@"dd/MM/yy"];
//        user.dob = [_formatter stringFromDate:date];
//            
//        NSString *sqlQuery = @"UPDATE User SET userId = ?,height = ?,weight = ?,ethnicity = ?,aboutMe = ?,status = ?,dob = ?,gender = ?,thumbnailURL = ?";
//        
//        const char *sql = [sqlQuery UTF8String];
//        if (sql == nil)
//        {
//            return;
//        }
//        if (sqlite3_prepare_v2(database, sql, -1, &init_statement, NULL) != SQLITE_OK) {
//            
//            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
//            
//        }
//        sqlite3_bind_int(init_statement, 1, user.userId);
//        sqlite3_bind_int(init_statement, 2, user.height);
//        sqlite3_bind_int(init_statement, 3, user.weight);
//        
//        sqlite3_bind_text(init_statement, 4, [user.ethnicity UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(init_statement, 5, [user.aboutMe UTF8String], -1,SQLITE_TRANSIENT);
//         sqlite3_bind_text(init_statement, 6, [user.status UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(init_statement, 7, [user.dob UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_text(init_statement, 8, [user.gender UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_text(init_statement, 9, [user.thumbnailURL UTF8String], -1, SQLITE_TRANSIENT);
//        success = sqlite3_step(init_statement);
//        
//        if (success == SQLITE_ERROR) {
//            
//            NSAssert1(0, @"Error: failed to update suggested friends Data into the database with message '%s'.", sqlite3_errmsg(database));
//            
//        }
//        
//    }
//    
//    @catch (NSException *exception) {
//        
//        if (success == SQLITE_ERROR) {
//            
//            NSAssert1(0, @"Error: failed to update User Data into the database '%s'.", sqlite3_errmsg(database));
//            
//            sqlite3_finalize(init_statement);
//        }
//    }
//    @finally {
//        if(success == SQLITE_ERROR) {
//            
//            NSAssert1(0, @"Error: failed to update User Data into the database '%s'.", sqlite3_errmsg(database));
//            
//        }
//        
//        sqlite3_finalize(init_statement);
//    }
//    
//}
//
//
//
//-(NSMutableArray *) executeQuery:(NSString *)query
//{
//    if (query != nil)
//    {
//    sqlite3_stmt *init_statement=nil;
//    
//    NSMutableArray *resultArray;
//    
//    @try
//    
//    {
//        SQLiteHelper *sqliteHelper =[SQLiteHelper sharedSingleton];
//        if (sqliteHelper != nil)
//        {
//            database = [sqliteHelper getDataBase];
//            if (database == nil)
//            {
//                return NO;
//            }
//        }
//        else
//        {
//            return NO;
//        }
//        resultArray = [[NSMutableArray alloc]init];
//        
//        
//        if(init_statement == nil)
//        
//        {
//            
//            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &init_statement, NULL) != SQLITE_OK){
//                
//                NSAssert1(0,@"Error: Failed to prepare statement with message '%s'.",sqlite3_errmsg(database));
//                
//            }
//            
//        }
//        
//        NSString *str;
//        
//        
//        while(sqlite3_step(init_statement)==SQLITE_ROW )
//        
//        {
//            
//            char *row;
//            
//            int columnCount = sqlite3_column_count(init_statement);
//            
//            for (int i=0; i<=columnCount; i++)
//            
//            {
//                
//                row = (char*)sqlite3_column_text(init_statement, i);
//                
//                if(row != NULL)
//                
//                {
//                    
//                    str = [NSString stringWithUTF8String:row];
//                    
//                    if([str length]>0)
//                    
//                    {
//                        
//                        [resultArray  addObject:str];
//                        
//                    }
//                    
//                    
//                }
//                
//            }
//            
//            
//        }
//        
//    }
//    
//    @catch (NSException *ex) 
//    
//    {
//        
//        NSLog(@"Error Occured while Retrieving information");
//        
//    }
//    
//    @finally 
//    
//    {
//        
//        
//        
//        if(init_statement)sqlite3_finalize(init_statement);
//        
//    }
//    
//    return resultArray;
//    
//}
//
//}
//- (BOOL)insertFocvsCacheDetails: (NSDictionary *)JsonWithAction
//{
//    if (JsonWithAction == nil)
//    {
//        return NO;
//    }
//    
//    sqlite3_stmt *init_statement=nil;
//    int success;
//    @try {
//        sqliteHelper =[SQLiteHelper sharedSingleton];
//        if (sqliteHelper != nil)
//        {
//            database = [sqliteHelper getDataBase];
//            if (database == nil)
//            {
//                return NO;
//            }
//            [sqliteHelper openDatabase:[sqliteHelper databasePath]];
//        }
//        else
//        {
//            return NO;
//        }
//        
//        NSString *str = @"INSERT INTO FocvsCache (action, response) VALUES(?,?)";
//        //        NSString *str = [NSString stringWithFormat:@"INSERT INTO %@ (action, response) VALUES(?,?)",KFocvsCacheTable];
//        
//        //        NSString *insertStatement = [NSString stringWithFormat:@"INSERT INTO USER (FIRSTNAME, LASTNAME) VALUES (\"%@\", \"%@\")", [JsonWithAction objectForKey:@"action"], [JsonWithAction objectForKey:@"response"]];
//        
//        //        char *error;
//        
//        const char *sql = [str UTF8String];
//        if (sql == nil)
//        {
//            return NO;
//        }
//        
//        if (sqlite3_prepare_v2(database, sql, -1, &init_statement, NULL) != SQLITE_OK) {
//            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
//            return NO;
//        }
//        
//        
//        sqlite3_bind_text(init_statement, 1, [[JsonWithAction objectForKey:@"action"] UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(init_statement, 2, [[JsonWithAction objectForKey:@"response"] UTF8String], -1,SQLITE_TRANSIENT);
//        
//        success = sqlite3_step(init_statement);
//        
//        if (success == SQLITE_ERROR) {
//            NSAssert1(0, @"Error: failed to insert FocvsCache Data into the database with message '%s'.", sqlite3_errmsg(database));
//            return NO;
//        }
//    }
//    @catch (NSException *exception) {
//        if (success == SQLITE_ERROR) {
//            NSAssert1(0, @"Error: failed to insert FocvsCache Data into the database with message '%s'.", sqlite3_errmsg(database));
//            return NO;
//        }
//        sqlite3_finalize(init_statement);
//    }
//    @finally {
//        if (success == SQLITE_ERROR) {
//            NSAssert1(0, @"Error: failed to insert FocvsCache Data into the database with message '%s'.", sqlite3_errmsg(database));
//            return NO;
//        }
//        sqlite3_finalize(init_statement);
//    }
//    return YES;
//}

@end

