//
//  Network.h
//  focvs
//
//  Created by Shubham Sharma on 17/12/14.
//  Copyright (c) 2014 Nishkrant Media. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkConstants.h"

@class LoginReq;
@class ForgotPasswordReq;
@class GetWallpaperReq;
@class RegisterReq;
@protocol NetworkDelegate <NSObject>

- (void) didReceiveResponse:(NSDictionary*)response withUserInfo:(NSDictionary*)userInfo;
- (void) didFailWithError:(NSError *)error withUserInfo:(NSDictionary*)userInfo;
@optional
- (void) didReceiveUploadingProgress:(float)value;
@end


@class AFHTTPClient;

static NSString * AWSS3BucketUrl = @"";

@interface Network : NSObject
@property(weak) id <NetworkDelegate> delegate;
@property(weak) AFHTTPClient *httpClient;

- (void) userLogin:(LoginReq *)req UserInfo:(NSDictionary*)userInfo;
- (void) userRegister:(RegisterReq *)req UserInfo:(NSDictionary*)userInfo;
- (void) forgotPasswordWS: (ForgotPasswordReq *) req UserInfo:(NSDictionary*)userInfo; // forgot password
- (void) checkSocailRegistration:(NSDictionary*)parameters UserInfo:(NSDictionary*)userInfo;
- (void) configSocailRegistration:(NSDictionary*)parameters UserInfo:(NSDictionary*)userInfo; // config Social account
- (void) getUserType; // Get User Type From WS
- (void) getWallpapers:(GetWallpaperReq *)parameters UserInfo:(NSDictionary*)userInfo; // get Wallpapers
- (void) userRegistration:(NSDictionary*)parameters;
@end
