//
//  Network.m
//  focvs
//
//  Created by Shubham Sharma on 17/12/14.
//  Copyright (c) 2014 Nishkrant Media. All rights reserved.
//

#import "Network.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "UserDefault.h"
#import "Loader.h"
#import "LoginReq.h"
#import "RegisterReq.h"
#import "ForgotPasswordReq.h"
#import "GetWallpaperReq.h"

@implementation Network

#pragma mark - login
- (void) userLogin:(LoginReq *)req UserInfo:(NSDictionary*)userInfo
{
    [self request:[[NSDictionary alloc] initWithObjectsAndKeys:req.action,KAction,req.user_name,KUserName,req.user_password,KPassword, nil] Path:[NSString stringWithFormat:@"%@",KServerBaseUrl] UserInfo:userInfo];
}


#pragma mark - register
- (void) userRegister:(RegisterReq *)req UserInfo:(NSDictionary*)userInfo
{
    [self request:[[NSDictionary alloc] initWithObjectsAndKeys:KRegistrationAction ,KAction,req.username,KUserName,req.firstName,KFirstName,req.lastName,KLastName,[NSString stringWithFormat:@"%@ %@",req.firstName,req.lastName],KFullName,req.emailId,KEmailId,req.phNumber,KPhNumber,req.gender,KGender,req.userType,KUserType,(req.fbId != nil ? req.fbId: @"0"),KFBId,req.gPlusId != nil ? req.gPlusId : @"0",KGPlusId,req.password,KPassword, nil] Path:[NSString stringWithFormat:@"%@",KServerBaseUrl] UserInfo:userInfo];
}

#pragma mark
#pragma mark - Forgot Password
-(void) forgotPasswordWS: (ForgotPasswordReq *) req UserInfo:(NSDictionary*)userInfo
{
    [self request:[[NSMutableDictionary alloc] initWithObjectsAndKeys:KForgotPasswordAction,KAction,req.emailIDOrUsername,KUsernameOrEmail, nil] Path:[NSString stringWithFormat:@"%@",KServerBaseUrl] UserInfo:userInfo];
}

#pragma mark - Check Social Registration

- (void) checkSocailRegistration:(NSDictionary*)parameters UserInfo:(NSDictionary*)userInfo
{
  [self request:parameters Path:[NSString stringWithFormat:@"%@",KServerBaseUrl] UserInfo:userInfo];
}

- (void) configSocailRegistration:(NSDictionary*)parameters UserInfo:(NSDictionary*)userInfo
{
    [self request:parameters Path:[NSString stringWithFormat:@"%@",KServerBaseUrl] UserInfo:userInfo];
}

#pragma mark - Get User types from WS
-(void)getUserType
{
    [self request:[[NSDictionary alloc] initWithObjectsAndKeys:KGetUserTypeAction,KAction, nil] Path:[NSString stringWithFormat:@"%@",KServerBaseUrl] UserInfo:[[NSDictionary alloc] initWithObjectsAndKeys:KGetUserTypeAction,KAction, nil]];
}

#pragma mark - get Wallpaper WS
- (void) getWallpapers:(GetWallpaperReq *)req UserInfo:(NSDictionary*)userInfo
{
    [self request:[[NSMutableDictionary alloc] initWithObjectsAndKeys:req.posts_per_page,KPostsPerPage,req.paged,KPaged, nil] Path:[NSString stringWithFormat:@"%@",KGetWallpaperURL] UserInfo:userInfo];
}
//api/core/get_posts/

- (void) userRegistration:(NSDictionary*)parameters // need to update the base url to "KServerServiceBaseUrl i.e http://play.nishkrant.com/focvs/api/api.php" and parameters to
{
//    [self request:parameters Path:[NSString stringWithFormat:@"%@",KServerServiceBaseUrl] UserInfo:[NSDictionary dictionaryWithObjectsAndKeys:kServerRegistration,kServerAPI, nil]];
    
//    [self request:parameters Path:[NSString stringWithFormat:@"%@",KServerServiceBaseUrl] UserInfo:[NSDictionary dictionaryWithObjectsAndKeys:kActionRegistration,KActionkey, nil]];
    
}


#pragma mark - HTTP Request Operation Manager
#pragma mark - POST URL-Form-Encoded Request
- (void) request:(NSDictionary*)parameters Path:(NSString*)path UserInfo:(NSDictionary*)userInfo
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"Parameters = %@",parameters);
        NSLog(@"Response = %@",responseObject);
        if ([self.delegate respondsToSelector:@selector(didReceiveResponse:withUserInfo:)])
        {
            [self.delegate didReceiveResponse:responseObject withUserInfo:userInfo];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if ([self.delegate respondsToSelector:@selector(didFailWithError:withUserInfo:)])
        {
            [self.delegate didFailWithError:error withUserInfo:userInfo];
        }
        

    }];
    
}
@end
