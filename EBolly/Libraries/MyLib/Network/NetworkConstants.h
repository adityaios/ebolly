//
//  NetworkConstants.h
//  focvs
//
//  Created by Shubham Sharma on 18/12/14.
//  Copyright (c) 2014 Nishkrant Media. All rights reserved.
//

#ifndef focvs_NetworkConstants_h
#define focvs_NetworkConstants_h


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)



#define kServerAPI @"api"

#define KServerBaseUrl @"http://cinepot.nishkrant.net/social/mobility/api.php"
#define KGetWallpaperURL @"http://cinepot.nishkrant.net/wallpaper?json=get_posts"



#define GPPUserInfoURL @"https://www.googleapis.com/oauth2/v1/userinfo?access_token=%@"

#define KHasLaunchedOnce @"HasLaunchedOnce"

#define KUserDetails @"user_details"

#define KResult  @"Result"
#define KResultWallpapers  @"result"
#define KUserID @"user_id"
#define KMsg @"msg"
#define KStatus @"status"
#define KStatusCode @"statusCode"

#define KSocialConfigAction @"configAccount"

#define KSocialLoginCheckAction  @"socialLoginCheck"
#define KFbId @"fb_id"
#define KGPlusId @"gplus_id"


#define KCheckSocialRegistrationAction @"checkSocialRegistrationAction"

#define KLoginType @"loginType"
#define KSocialType @"socialType"
#define KSocialRegister @"social"
#define KStandardRegister @"standard"
#define KFacebook @"facebook"
#define KGooglePlus @"googlePlus"
#define kProvider @"provider"

#define KFBId @"fb_id"
#define kGplusId @"gplus_id"

#define  KYes @"YES"
#define  KNo @"NO"

#define KAction @"action"

// Register
#define KRegistrationAction @"registration"
#define KuserType @"user_type"
#define KFirstName @"first_name"
#define KLastName @"last_name"
#define KUsername @"user_name"
#define KEmailId @"email_id"
#define KPhNumber @"phone"
#define KGender @"gender"
#define KPassword @"password"
#define KFullName @"full_name"
#define KVerifyPassword @""

#define KGenderMale @"1"
#define KGenderFemale @"0"

// Get User Types
#define KGetUserTypeAction @"getUserType"
#define KUserType @"userType"
#define KUserTypeId @"userTypeId"
#define KUserTypeName @"userTypeName"

// Login Actions
#define KLoginAction @"login"
#define KUserName @"user_name"
#define KPassword @"user_password"

// Forgot Password
#define KForgotPasswordAction @"forgotPassword"
#define KUsernameOrEmail @"user_name"

// Get Wallpapers
#define KGetPostsAction @"get_posts"
#define KPostsPerPage @"posts_per_page"
#define KPaged @"paged"

#define KImagesArray @"images_array"
#define KPostThumbnail @"post-thumbnail"
#define KThumbnail @"thumbnail"
#define KLarge @"large"
#define KUrl @"url"

#define KPageIndex @"pageIndex"

// UserId
#define kUserId @"user_id"

#endif
