//
//  UserDefault.h
//  focvs
//
//  Created by Shubham Sharma on 16/12/14.
//  Copyright (c) 2014 Nishkrant Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefault : NSObject
+ (void) setUserId:(NSString*)value;
+ (NSString*) getUserId;
+ (void) removeAllUserDefaults;
@end
