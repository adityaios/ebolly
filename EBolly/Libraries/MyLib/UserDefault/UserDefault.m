//
//  UserDefault.m
//  focvs
//
//  Created by Shubham Sharma on 16/12/14.
//  Copyright (c) 2014 Nishkrant Media. All rights reserved.
//

#import "UserDefault.h"
#import "NetworkConstants.h"


@implementation UserDefault

+ (void) setUserId:(NSString*)value
{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:kUserId];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString*) getUserId
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kUserId];
}


#pragma mark - Clear User Defaults

+ (void) removeAllUserDefaults
{
    NSString *restoringKey1 = [[NSUserDefaults standardUserDefaults]objectForKey:@"WebKitLocalStorageDatabasePathPreferenceKey"];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
    [[NSUserDefaults standardUserDefaults]setObject:restoringKey1 forKey:@"WebKitLocalStorageDatabasePathPreferenceKey"];
}



@end
