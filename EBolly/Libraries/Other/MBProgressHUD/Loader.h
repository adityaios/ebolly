//
//  Loader.h
//  focvs
//
//  Created by Shubham Sharma on 16/12/14.
//  Copyright (c) 2014 Nishkrant Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Loader : NSObject
+ (void) showLoader;
+ (void) hideLoader;
+ (void) showSuccessMessage:(NSString*)message;
+ (void) showFailedMessage:(NSString*)message;

@end
