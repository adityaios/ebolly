//
//  Loader.m
//  focvs
//
//  Created by Shubham Sharma on 16/12/14.
//  Copyright (c) 2014 Nishkrant Media. All rights reserved.
//

#import "Loader.h"
#import "MBProgressHUD.h"
#import "BasicUtilities.h"

@implementation Loader
+ (void) showLoader
{
    [[[[UIApplication sharedApplication] delegate] window] endEditing:YES];
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:[[[UIApplication sharedApplication] delegate] window]];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:hud];
    hud.labelText = @"Processing...";
    
    //rahul
    
    hud.color = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.6];
//    hud.labelColor = [UIColor colorWithRed:0.0/0.0 green:0.0/0.0 blue:0.0/0.0 alpha:0.6];

    hud.layer.borderColor = [UIColor blackColor].CGColor;
    hud.layer.borderWidth = 1.0;
    
    UIImageView * imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"logo.png"]];
    imgView.frame = CGRectMake(100, 200, 30, 30);
    [BasicUtilities runSpinAnimationOnView:imgView duration:50.0 rotations:1.0 repeat:INFINITY];
    hud.customView  = imgView;
    
    // Set custom view mode
    hud.mode = MBProgressHUDModeCustomView;
    hud.animationType = MBProgressHUDAnimationZoomIn;
    
    [hud show:YES];
}



+ (void) hideLoader
{
    [MBProgressHUD hideAllHUDsForView:[[[UIApplication sharedApplication] delegate] window] animated:YES];
}


+ (void) showSuccessMessage:(NSString *)message
{
    [self hideLoader];
    [[[[UIApplication sharedApplication] delegate] window] endEditing:YES];
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:[[[UIApplication sharedApplication] delegate] window]];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:HUD];
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check"]];
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeCustomView;
    HUD.animationType = MBProgressHUDAnimationZoomIn;    
    HUD.detailsLabelText = message;
    
    [HUD show:YES];
    [HUD hide:YES afterDelay:2];
}
+ (void) showFailedMessage:(NSString *)message
{
    [self hideLoader];
    [[[[UIApplication sharedApplication] delegate] window] endEditing:YES];
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:[[[UIApplication sharedApplication] delegate] window]];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:HUD];
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cross"]];
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeCustomView;
    HUD.animationType = MBProgressHUDAnimationZoomIn;
    HUD.detailsLabelText = message;
    
    [HUD show:YES];
    [HUD hide:YES afterDelay:2];
}

@end
