//
//  ForgotPasswordReq.h
//  EBolly
//
//  Created by Yashvir on 11/08/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import "JSONModel.h"

@interface ForgotPasswordReq : JSONModel
@property(nonatomic, strong) NSString * emailIDOrUsername;
@end
