//
//  GetWallpaperReq.h
//  EBolly
//
//  Created by Yashvir on 13/08/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import "JSONModel.h"

@interface GetWallpaperReq : JSONModel

@property (nonatomic, strong) NSString * posts_per_page;
@property (nonatomic, strong) NSString * paged;

@end
