//
//  LoginReq.h
//  EBolly
//
//  Created by Yashvir on 03/08/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import "JSONModel.h"

@interface LoginReq : JSONModel

@property(nonatomic, strong) NSString * action;
@property (nonatomic, strong) NSString * user_name;
@property (nonatomic, strong) NSString * user_password;

@end
