//
//  RegisterReq.h
//  EBolly
//
//  Created by Yashvir on 03/08/15.
//  Copyright (c) 2015 Nishkrant Media. All rights reserved.
//

#import "JSONModel.h"

@interface RegisterReq : JSONModel
@property(nonatomic, strong) NSString * userType;
@property(nonatomic, strong) NSString * firstName;
@property(nonatomic, strong) NSString * lastName;
@property(nonatomic, strong) NSString * username;
@property(nonatomic, strong) NSString * emailId;
@property(nonatomic, strong) NSString * phNumber;
@property(nonatomic, strong) NSString * gender;
@property(nonatomic, strong) NSString * password;
@property(nonatomic, strong) NSString * verifyPassword;
@property(nonatomic, strong) NSString * fbId;
@property(nonatomic, strong) NSString * gPlusId;

@end
